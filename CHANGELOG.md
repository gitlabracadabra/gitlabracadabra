# Change Log

## [2.6.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v2.5.1...v2.6.0) (2025-03-10)

### :sparkles: Features

* build deb on bookworm ([df37d1d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/df37d1dcc3735431b9c6fca938bc1fdffd58041e))
* build deb on bookworm and drop Debian bullseye images ([43f91c3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/43f91c39aec11874c1b1cec59caae16d75c46da8))
* drop Debian bullseye images ([318d4f8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/318d4f8a1e25bf2e23ea54095d4a36110784219e))
* **members:** do not remove bots ([b8cbb13](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b8cbb13e5803bcb92fac9b67bc4b931f4ecc37a8))
* **members:** do not remove bots ([3524eca](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3524ecafc7172374fae350e7d8ee6993e541db1e))
* mirrors, package mirrors and image mirrors after the rest ([0855bcb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0855bcbb1ae9fb693c27f83cb902be18e5b3b756))
* mirrors, package mirrors and image mirrors after the rest ([956c7e9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/956c7e97be9c1f22e7a2a2ee2cb9846f5fe3b76d))

### :bug: Fixes

* **protected-branches:** avoid double None ([5014468](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5014468a9ef827b61feb2947ed6581a56a6cda64))
* **protected-branches:** avoid double None ([7a09491](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7a094914721cc4659a0be14c1a1bc3e1aa7c9a5f))

### :memo: Documentation

* container expiration policy accepted older_than ([3ea4740](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3ea47405b436f1e5fe9df6621926d99609312849))
* container expiration policy accepted older_than ([b1a6083](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b1a608314c6ff8a079e0e5df01e343bcc79205b4))
* update dev setup ([1b764de](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1b764de3679e21a0b09b7bd6cba4559de32aebce))
* update generated docs ([7d672f5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7d672f59a0c5b5ec222f0cf6fc96d061cc1b5b73))
* update release docs ([ba35563](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ba35563b0ee0133f920ad91580f509229f3bf436))

### :zap: Refactor

* avoid negated condition ([a8170a8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a8170a84ba1e64c860ef85d82a434728b6a83471))
* **members:** reduce indentation ([9ad54fc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9ad54fccb6f74319c898ae0dd98519193847977a))
* reduce mypy complains ([aeb8465](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/aeb8465e117c41ec7d059645c971863cf09d7dd5))

### :repeat: CI

* automatic fixes by ruff ([6619be6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6619be634853c56d15e752fc401d16930dfbac7a))
* fix ARG001 Unused function argument: `kwargs` ([5f05a29](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5f05a29b848624b2b4d9ebb318c091c1b29f93d5))
* fix ARG002 Unused method argument ([f36fd11](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f36fd11c3d50b8556a78d192bd097484b1e4e89f))
* fix B904 Within an `except` clause, raise exceptions with `raise ... from err` ([143c72a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/143c72ad6caf84b5d6679247672aa43c8c371ad8))
* fix EXE001 Shebang is present but file is not executable ([9d1c0a5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9d1c0a52d5fd8917957013db0f47a7e36ecb7eda))
* fix FBT001 Boolean-typed positional argument in function definition ([c415ee6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c415ee678532f3735eb88f923107e244e5d850ff))
* fix FBT002 Boolean default positional argument in function definition ([4232a7f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4232a7f83c3cb38b8810b239d6284dd73d777f96))
* fix ISC003 Explicitly concatenated string ([5636ead](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5636ead1437a0366dc6ef78c97fe6e6725693798))
* fix PERF401 Use a list comprehension to create a transformed list ([b8fc5ce](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b8fc5ce4a0adc649c9fe72d55e81fc205dcbec74))
* fix PLR2004 Magic value used in comparison ([6832a8c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6832a8c42d1048b415fffb8ba091076585a0dce2))
* fix PT009 Use a regular `assert` instead of unittest-style `assertListEqual` ([ec7b58c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ec7b58c0501354aa0a57dbd7b70a37309510151a))
* fix PT012 `pytest.raises()` block should contain a single simple statement ([0fb42e5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0fb42e576d0f5dd377dee11fd9bbe41687fe6240))
* fix PT027 Use `pytest.raises` instead of unittest-style `assertRaises` ([9104038](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9104038d38d8a1bb459101b9d52b2a35dcab6bb1))
* fix PYI024 Use `typing.NamedTuple` instead of `collections.namedtuple` ([155e846](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/155e8461167047a5fc9b611079ffdfe7e4da2a72))
* fix PYI034 `__enter__` methods in classes like `WithDigest` usually return `self` at runtime ([d05f55d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d05f55dabae1af311fb7ee8ba8be56a8ee2d5865))
* fix RET505 Unnecessary `else/elif` after `return` statement ([2349e9c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2349e9cb37f43811156b3c00727521b8a36ed61d))
* fix RUF001 String contains ambiguous character ([435b819](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/435b8196e11c109aeb05e5d90993a3d5e0a95d19))
* fix RUF012 Mutable class attributes should be annotated with `typing.ClassVar` ([272b4d5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/272b4d5c04a54320ea9d3bcf68af31574cb8b7ed))
* fix SIM117 Use a single `with` statement ([0bbfd7c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0bbfd7c4d8eef3bbca01d5df65e45a9a16298d82))
* fix SLF001 Private member accessed ([cf029fe](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cf029fe4e97da896797e00a8989b1ed57941ed99))
* fix TD004 Missing colon in TODO ([d2b09f4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d2b09f4e4b1dfd99e04589a78e0b1e28458a1ec9))
* fix TRY004 Prefer `TypeError` exception for invalid type ([2de2fb8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2de2fb8d1fc13214d9ec92ad4871abf1c64e360d))
* fix TRY300 Consider moving this statement to an `else` block ([fdcff0f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fdcff0f0507923ee01c42ff6ab1c663b63c71949))
* fix UP007 Use `X | Y` for type annotations ([b5679af](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b5679af96a549b6c19b035c31765ded24cbb0ddf))
* fix UP013 Convert `PackageFileArgs` from `TypedDict` functional to class syntax ([f0f410a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f0f410a236db7c89840415d8df7cd54a76e3685e))
* fix UP030 Use implicit references for positional format fields ([76f631d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/76f631d02789c8c8098254992a6c9d57ed424051))
* ignore B018 Found useless expression ([d8d828f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d8d828f00c1cffd1653e341d82986284fd7a5fb3))
* ignore BLE001 Do not catch blind exception: `Exception` ([f996a56](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f996a56bed6583891031f37c8e8522a079e4372a))
* ignore PLW2901 `for` loop variable overwritten by assignment target ([312bdbf](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/312bdbf99c8b6a52099138e60d7db7f44538f1ac))
* ignore PT011 `pytest.raises(ValueError)` is too broad ([ab0fcb7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ab0fcb79e150313c75792d7c30db982f59397f59))
* ignore SIM115 Use context handler for opening files ([533023c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/533023c907be4386f027e62e5cd36a46e43dfd19))
* ignore some ruff rules ([54f14d5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/54f14d50cc89063cc826216df62328f7346b5b56))
* ignore T201 `print` found ([ca074d6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ca074d6ba45062ce34db028d8c6398c62005c963))
* make mypy and ruff happy together ([b0d16f5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b0d16f54ce0da83833d5e75248f94c20d3612958))
* re-test almost all project params ([83d9914](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/83d99140b44b78cbe90db555b1260c7465fa6e49))
* re-test almost all project params ([4de2b3d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4de2b3d15e50b8cc5197ca068e4f77bf02716735))
* remove tox.ini ([d1a0c33](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d1a0c33822a600b41d037049765702e0e4e16e0f))
* run hatch fmt ([842ed17](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/842ed176ab5768e69f2cc9ca921c84f8f4c9c176))
* test with hatch ([f1d3a84](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f1d3a84d59638a1d52546b89e91ab149a9fba72c))
* unsafe automatic fixes by ruff ([ad89faa](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ad89faa8b96825e3ca01a217db7c0de9b5c13e07))
* update test image ([f0b681e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f0b681e6abd07ec4aa155383a54f570ff337e9b0))

### :repeat: Chore

* add .venv to docker ignore ([8c577fb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8c577fb6734dabc722d5a01541686bc7230f1d67))
* **config:** really pin urllib3 ([ae90407](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ae90407538bfc369d6b4f0e7dfca0914b60e878a))
* **deps:** update alpine docker tag to v3.21.0 ([09ab682](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/09ab682acf59a79ead8deb5ba0fec7828544edb8))
* **deps:** update alpine docker tag to v3.21.0 ([fc7ca8a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fc7ca8ac4c654a42c0bb40a72b963d899149a973))
* **deps:** update alpine docker tag to v3.21.1 ([c74350e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c74350e11b2f3cf18934660368094e47a2be97f4))
* **deps:** update alpine docker tag to v3.21.1 ([734121a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/734121a058515313c0b865efd34cb689421b49fc))
* **deps:** update alpine docker tag to v3.21.2 ([c76074e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c76074e4184129f0ddf1134db0a6fe73b2416853))
* **deps:** update alpine docker tag to v3.21.2 ([6929669](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6929669bd3a18635f4e2e34f64cf702dbd73458c))
* **deps:** update alpine docker tag to v3.21.3 ([a824a8c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a824a8c338b885d2a2b2b29286a6fa58c45f9e3a))
* **deps:** update alpine docker tag to v3.21.3 ([7fd5d1c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7fd5d1c041eeccd3fb374243985652de776cb565))
* **deps:** update debian:bookworm docker digest to 17122fe ([46c8126](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/46c81262b1ddff1fad652be6857fdf236dbfd357))
* **deps:** update debian:bookworm docker digest to 17122fe ([5328a35](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5328a353c702aefaafdf4dc9d727a58d21894af2))
* **deps:** update debian:bookworm docker digest to 3213417 ([c2e9d4f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c2e9d4fb048ed19f682d913a118053a08fe3a8b3))
* **deps:** update debian:bookworm docker digest to 3213417 ([c8dcf28](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c8dcf2863b4e76554f4150f7ae890ccbaf77d339))
* **deps:** update debian:bookworm docker digest to 3528682 ([402d601](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/402d6019fdaacd6bcef39442868f7a3b11207a1c))
* **deps:** update debian:bookworm docker digest to 3528682 ([c0c59a7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c0c59a7c1703ff99ede9a739f3d67daf903811c4))
* **deps:** update debian:bookworm docker digest to 4abf773 ([dd6e55e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dd6e55ed853893ea1cadf3e6eafad7681e2097f9))
* **deps:** update debian:bookworm docker digest to 4abf773 ([0496782](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/049678230b73805e8bb50d261425db7c9b38f91a))
* **deps:** update debian:bookworm docker digest to b877a1a ([f59d939](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f59d93906ffef15264897003cd98441135ee7cc4))
* **deps:** update debian:bookworm docker digest to b877a1a ([00390cd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/00390cd70dfbf7e5eb0637ea05146523353da1d5))
* **deps:** update debian:bookworm-slim docker digest to 12c396b ([7ace787](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7ace787aa000632df381acfdb48c452c3b39b2bb))
* **deps:** update debian:bookworm-slim docker digest to 12c396b ([dcec81f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dcec81fa7b5c52e9e50ed86f37b1b2cb8811011b))
* **deps:** update debian:bookworm-slim docker digest to 1537a6a ([a42d111](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a42d1118bd43dfbb269c9d0b18b4e1daaf98386d))
* **deps:** update debian:bookworm-slim docker digest to 1537a6a ([c2cba41](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c2cba4112b84014890fc4e9f84109b334c76206d))
* **deps:** update debian:bookworm-slim docker digest to 40b1073 ([a811356](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a811356cd7c903fb137c4d71c5dc045a3ca45936))
* **deps:** update debian:bookworm-slim docker digest to 40b1073 ([d440b76](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d440b764a4af647a0ae9043faf26af9138bbe423))
* **deps:** update debian:bookworm-slim docker digest to d365f49 ([24e5767](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/24e5767bb4c54f107409401b58c89f669ee9675c))
* **deps:** update debian:bookworm-slim docker digest to d365f49 ([38be587](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/38be58708a69d9a2e37b05566f697e73d19df3b2))
* **deps:** update debian:bookworm-slim docker digest to f70dc8d ([8c4626f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8c4626fa8af40320c658c5a6fb3165463802b105))
* **deps:** update debian:bookworm-slim docker digest to f70dc8d ([dc9cfca](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dc9cfca9f192afc7695ddea7d3402c80037dbf94))
* **deps:** update debian:bullseye docker digest to 21b74d9 ([fd16910](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fd169101e6a78579b15e5031f5cb336037b86bd9))
* **deps:** update debian:bullseye docker digest to 21b74d9 ([ef27af1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ef27af1ac0e083ef77cf922c6dc9578c34bd5a8e))
* **deps:** update debian:bullseye docker digest to e5bfb73 ([767c9f2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/767c9f2a69a99b7b3966a4b13f30efc176790419))
* **deps:** update debian:bullseye docker digest to e5bfb73 ([cbb2e46](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cbb2e469135b74bd0043320faa634cada7e8b3e6))
* **deps:** update debian:bullseye docker digest to e91d1b0 ([6ed245d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6ed245dfc37339cfeefdd9d07b7a6d38b23b0c52))
* **deps:** update debian:bullseye docker digest to e91d1b0 ([2c8a66b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2c8a66bcc772baba22b5d082cd82a6fe05f7876a))
* **deps:** update dependency bandit to v1.8.0 ([2dafa9d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2dafa9d4d5c6e871cf232442a5cc789d5b09ae52))
* **deps:** update dependency bandit to v1.8.0 ([e1d211d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e1d211d41d0147691f91aed351c2e7b7312d4b9f))
* **deps:** update dependency bandit to v1.8.2 ([dd53437](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dd53437954d9d2ed01bb93e97fa18fa3775b9f28))
* **deps:** update dependency bandit to v1.8.2 ([b30f380](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b30f380828e8206764b5efa5622a40ddb820d3db))
* **deps:** update dependency bandit to v1.8.3 ([c9a253a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c9a253a730387e738648a3a73672077612386eff))
* **deps:** update dependency bandit to v1.8.3 ([ad75b58](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ad75b58676689340f4f4f01a8e71d10a12046166))
* **deps:** update dependency flake8-commas to v4 ([8ad7668](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8ad7668accad9a46a5d2aaea162eb489a58fde26))
* **deps:** update dependency flake8-commas to v4 ([c26fbb5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c26fbb5e94b2e5c4f07746bb77852881511fa277))
* **deps:** update dependency isort to v6 ([592f013](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/592f0133913b97d12e518609149069e63c8a110d))
* **deps:** update dependency isort to v6 ([bff2307](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bff23077557700a76575861475980efacf5944a6))
* **deps:** update dependency isort to v6.0.1 ([a57308b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a57308b2b6ffdf3b34233457c4814bea424c2b25))
* **deps:** update dependency isort to v6.0.1 ([405bff0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/405bff0cc2984fb4a89f6fa2df9f4656a75a95ba))
* **deps:** update dependency mypy to v1.14.0 ([8ec99e9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8ec99e980ec42705d219d0ae955f957dc3b8eade))
* **deps:** update dependency mypy to v1.14.0 ([fd5d42c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fd5d42c2df3e2db976fa32680e6e7e4ce544641a))
* **deps:** update dependency mypy to v1.14.1 ([2daa35f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2daa35f0de32c66201ee1de53ba545e21929ddf3))
* **deps:** update dependency mypy to v1.14.1 ([cc6764f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cc6764f9feb30e5e964e34b95423acae2587c466))
* **deps:** update dependency mypy to v1.15.0 ([8fa702a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8fa702aa03a0e46dd64bf751b9601c0047be43af))
* **deps:** update dependency mypy to v1.15.0 ([1d89020](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1d89020b795e5891d1902d04d7b513c14755a65a))
* **deps:** update dependency pygit2 to v1.17.0 ([8478ff7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8478ff709d1a5cca614ee9597ac66cb287054301))
* **deps:** update dependency pygit2 to v1.17.0 ([8c0e7fe](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8c0e7fe541c07627d9a31756228e9e9dd2c02b25))
* **deps:** update dependency pygithub to v2.6.0 ([61b3f93](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/61b3f93ec0da6b9566fcc3ede03786187b39696a))
* **deps:** update dependency pygithub to v2.6.0 ([d92c6b8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d92c6b8c3d301eb4503b02f5c50618632841377a))
* **deps:** update dependency pygithub to v2.6.1 ([53f99c7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/53f99c7fedf928beed352a9d2425a46b2d1f8ced))
* **deps:** update dependency pygithub to v2.6.1 ([4cd2676](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4cd26765425d085d0bb2c0187d04ec419439bce0))
* **deps:** update dependency pylint to v3.3.2 ([9568b3d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9568b3dd0098f1fc47db39fdd15cad7bfb585893))
* **deps:** update dependency pylint to v3.3.2 ([95dabf2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/95dabf248dbd196bb2fc09196f6c19417ceca1f6))
* **deps:** update dependency pylint to v3.3.3 ([7f4b9c6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7f4b9c6e026282b3164396645eb8defc46d3e7a8))
* **deps:** update dependency pylint to v3.3.3 ([c70c904](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c70c904533a40188dc82d9c3b8ba5638b5eff9d0))
* **deps:** update dependency pylint to v3.3.4 ([9096e0b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9096e0b030af5d1d2d3aec8429686898d5574cc3))
* **deps:** update dependency pylint to v3.3.4 ([2b2ae20](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2b2ae20baa01d0c6c227728adef38dd6d0e35c79))
* **deps:** update dependency pytest to v8.3.4 ([a807193](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a8071931f7c6899d43435d8d7d01657f8e0ba20a))
* **deps:** update dependency pytest to v8.3.4 ([788cdb3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/788cdb34ca17bf046d9337c6876adfc753ee7dc9))
* **deps:** update dependency pytest to v8.3.5 ([26f2189](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/26f218909dd8f8a2e653d926da7ef6bda097f1fe))
* **deps:** update dependency pytest to v8.3.5 ([9ee2c0e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9ee2c0e66275358f27f50eadb411d7abc15c6a67))
* **deps:** update dependency python-gitlab to v5.1.0 ([26eb47c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/26eb47c1a8b4994b1ad861bf70364cbfca586fa3))
* **deps:** update dependency python-gitlab to v5.1.0 ([148d5d0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/148d5d018890ec3f4594fd24974728ef6f6d75cd))
* **deps:** update dependency python-gitlab to v5.2.0 ([ca5d20b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ca5d20ba9da336cc3aa6d6f1381cc194b834a2ab))
* **deps:** update dependency python-gitlab to v5.2.0 ([a99c822](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a99c822cbd11c9c14d6b50fdf231837a5a0b37c8))
* **deps:** update dependency python-gitlab to v5.3.0 ([0f754a1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0f754a191ccfb256af2faab0e8097c682ad1042f))
* **deps:** update dependency python-gitlab to v5.3.0 ([1453c23](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1453c2367aee7969c4b963ea1c0a27db5778d2a0))
* **deps:** update dependency python-gitlab to v5.3.1 ([994e01a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/994e01a3d10ec45eaa575595482af38302683805))
* **deps:** update dependency python-gitlab to v5.3.1 ([b5f0e45](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b5f0e4556cd502f3d19ac0df55d1b97f8f673e79))
* **deps:** update dependency python-gitlab to v5.4.0 ([3af6e23](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3af6e23d8f384e199509e97f8ab0ed6df6d3bfd3))
* **deps:** update dependency python-gitlab to v5.4.0 ([a90ece3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a90ece32d5c5d92d30cf6eebd4bdc9fb09c703af))
* **deps:** update dependency python-gitlab to v5.6.0 ([b35ff24](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b35ff240ba94242b89005aa5c5457da655f1ff86))
* **deps:** update dependency python-gitlab to v5.6.0 ([1915244](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1915244fb7dd463dbee9cbbcf59b33b39b717289))
* **deps:** update dependency types-pyyaml to v6.0.12.20241221 ([dc3c22b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dc3c22b3b892f2cb97e346e64108e5820a44d9e6))
* **deps:** update dependency types-pyyaml to v6.0.12.20241221 ([4a931e9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4a931e91965c8ae38d3ebfc7fa6347015fcee5a8))
* **deps:** update dependency types-pyyaml to v6.0.12.20241230 ([d3aa1c4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d3aa1c4f8c6abd99444eb30604eb9586bab51483))
* **deps:** update dependency types-pyyaml to v6.0.12.20241230 ([a384761](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a384761b94dd7b6324b56fc3838a3d31a71e0c5e))
* **deps:** update dependency types-requests to v2.32.0.20250301 ([ae9661d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ae9661dccd229a52a4cebc1143f4362b91cabc24))
* **deps:** update dependency types-requests to v2.32.0.20250301 ([c3d9285](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c3d92854586e899bb770a4795bed6f7b1757b208))
* **deps:** update dependency urllib3 to v2.3.0 ([c12e06d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c12e06d699520d382b7da4449a0a8d29867f8e08))
* **deps:** update dependency urllib3 to v2.3.0 ([465b138](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/465b138421ad78fd161647af16d5dcba731b183f))
* **deps:** update dependency vcrpy to v7 ([0441360](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/044136033bd78a8ae15de634fac94d3f1b8ee4aa))
* **deps:** update dependency vcrpy to v7 ([0f39cb7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0f39cb7c3f1800544716524752305f35ecca1a8a))
* **deps:** update dependency wemake-python-styleguide to v1 ([c8cf1b4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c8cf1b4d329c785da556c7dc2607335db33a25b6))
* **deps:** update dependency wemake-python-styleguide to v1 ([06f5b03](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/06f5b037ad285e2c49cfcdc67ecb60db78410244))
* **deps:** update docker docker tag to v28 ([ad84408](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ad84408b50486272baae6919d768bc54ff3c4d5a))
* **deps:** update docker docker tag to v28 ([e99305d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e99305d2fcb7c874c90e572a6fa0802305ff6ee7))
* **deps:** update docker:27-dind docker digest to 2711aa7 ([d0f17fc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d0f17fcbdf1d71c3fadc923635bb187511a28130))
* **deps:** update docker:27-dind docker digest to 2711aa7 ([787cb06](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/787cb068b7f92f4212185bed688207d9b685a6d2))
* **deps:** update docker:27-dind docker digest to 3ab005a ([141c603](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/141c603725ab7b1ecf1a726d0e3564e5fe70e61a))
* **deps:** update docker:27-dind docker digest to 3ab005a ([72007f2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/72007f2f806a4d4df9cc53957035ed0a13a2b838))
* **deps:** update docker:27-dind docker digest to 45b80ef ([370c161](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/370c1613e06fdf4fdd56b1aee688ea77fdcd5593))
* **deps:** update docker:27-dind docker digest to 45b80ef ([0ccf720](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0ccf72075dfc0c1484d4c97c3657ac78e24f5027))
* **deps:** update docker:27-dind docker digest to 716681c ([c793306](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c793306a25fdbbd1d3c6f2d602e53e1b1fd7b85f))
* **deps:** update docker:27-dind docker digest to 716681c ([a2abe03](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a2abe030dca601cc298210d2b4989ededf6716e8))
* **deps:** update docker:27-dind docker digest to a6cd14b ([63261e7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/63261e7e212a98940824611eb24608758490d214))
* **deps:** update docker:27-dind docker digest to a6cd14b ([dfe900c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dfe900ca9a6975e56f4cb749c3398c63402b3713))
* **deps:** update docker:27-dind docker digest to aa3df78 ([bf86c5c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bf86c5c4f4034ba5d3899edee7703e093a427646))
* **deps:** update docker:27-dind docker digest to aa3df78 ([adf9a11](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/adf9a11e4a3c9b5ac982c0dc6b70cd3f37630a44))
* **deps:** update docker:27-dind docker digest to b0c1179 ([5c6a676](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5c6a676c98c0a385569799461856627f23d242fa))
* **deps:** update docker:27-dind docker digest to b0c1179 ([6d5f3b6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6d5f3b61fcdd026c8c02d7ef7a7bdc0e7c3566f7))
* **deps:** update docker:27-dind docker digest to ba55987 ([f274055](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f274055fccac1c75e08b458104dd091ef1ff0b6a))
* **deps:** update docker:27-dind docker digest to ba55987 ([5f305b5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5f305b5506ad6345f930d4ab58a82778f38bb536))
* **deps:** update docker:27-dind docker digest to cbde039 ([105fa62](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/105fa62e8d620a4392237820b1541bfd327d3968))
* **deps:** update docker:27-dind docker digest to cbde039 ([f7c6e3d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f7c6e3d11eace23c5722f6a5b5eb8c7c8224e616))
* **deps:** update docker:27-dind docker digest to d33ffba ([f883b32](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f883b3260183aee6fd08b848f07bf83216cd2822))
* **deps:** update docker:27-dind docker digest to d33ffba ([edc8645](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/edc86459473e19f031ab6206927010005604f56a))
* **deps:** update docker:28-dind docker digest to 9a651b2 ([a67ed4c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a67ed4cf64937f47cb9ca7bb82bd771b0504ec81))
* **deps:** update docker:28-dind docker digest to 9a651b2 ([1d02595](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1d025956001fdb93889b8552343d19913474bdf2))
* **deps:** update node.js to 0e910f4 ([ef3de5f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ef3de5fbae7eb2617cf1438aad584543dfee294d))
* **deps:** update node.js to 0e910f4 ([32560ff](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/32560ff16ff31bcb20311c4fc164d7f17e7eaf53))
* **deps:** update node.js to 4f7fb7f ([210d46e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/210d46e754718dd53d67a4e25b78d0b87a6b137b))
* **deps:** update node.js to 4f7fb7f ([9bcc13b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9bcc13b59f4cecfe6526b29d42434b27269dd22d))
* **deps:** update node.js to 5145c88 ([edbfa5a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/edbfa5a348ac46a3e99354f9013a3ae035caacda))
* **deps:** update node.js to 5145c88 ([abd6138](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/abd61387b37c44247bea4337889aeffd146eec16))
* **deps:** update node.js to a279671 ([6fd2364](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6fd2364c6849d678354258c5a4de109ebaf9cc80))
* **deps:** update node.js to a279671 ([787af88](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/787af885fd7bd9e26b86b5873581b9bc274fd97d))
* **deps:** update node.js to cb24453 ([f54f372](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f54f372f991c0b37d88313878786acf49fe8dcd9))
* **deps:** update node.js to cb24453 ([82b2a95](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/82b2a95af7dd0b49e8b7740c9433ccdbf78b75ce))
* **deps:** update node.js to ec878c7 ([dc5fbb8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dc5fbb8d8ea6a1b26a71ba3a6d9629ecb0b81354))
* **deps:** update node.js to ec878c7 ([ade7bcc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ade7bcc56f640de6980b3287ccc620ceb8e615a9))
* **deps:** update node.js to f6b9c31 ([3436631](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3436631a7920707e6f1b2ce87e0d796c7ab0cc36))
* **deps:** update node.js to f6b9c31 ([b53079a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b53079a079dcbbf95cfb684e5d7eac7d2d4d74da))
* **deps:** update node.js to fa54405 ([ca48881](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ca488812e90d6c5afe92d2afb6d58a9291fa6bb3))
* **deps:** update node.js to fa54405 ([c1a138d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c1a138db681ec8841ec775e4461fab4f0f07483e))
* **deps:** update node.js to v22.12.0 ([9d90548](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9d905488b9920c582e7be36d0243228cd0620034))
* **deps:** update node.js to v22.12.0 ([de422ac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/de422acc2127386db36e2fd57d83f2406816ecf9))
* **deps:** update node.js to v22.13.0 ([290a7e5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/290a7e54a232f72cd4d863290d26912c84df817a))
* **deps:** update node.js to v22.13.0 ([78dc0c3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/78dc0c3c8d80b5f9ee30b9b8d2795c7d6e4b1eec))
* **deps:** update node.js to v22.13.1 ([4c105b3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4c105b358f77c799e11742437d985dc27d759d5f))
* **deps:** update node.js to v22.13.1 ([a260650](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a260650efb419291696842b9b369b25c8ba862cc))
* **deps:** update node.js to v22.14.0 ([4e79543](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4e7954319f2255dfc7c0860ccdab617bdf625048))
* **deps:** update node.js to v22.14.0 ([72965f6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/72965f69e9fc29cead34040df69596daab160375))
* **deps:** update python:3-slim-bookworm docker digest to 026dd41 ([1428658](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1428658ea51fffb66feddbebcdff71ccb56deedf))
* **deps:** update python:3-slim-bookworm docker digest to 026dd41 ([00bdabe](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/00bdabe5d5cda9a8c57a4cfc564ea6506293dcc1))
* **deps:** update python:3-slim-bookworm docker digest to 0911c0f ([a76461a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a76461aecbda60e1ec27d9c33b2f2674f9bfa78d))
* **deps:** update python:3-slim-bookworm docker digest to 0911c0f ([42990d1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/42990d1a83779d8108208ef260839aba9975e5e5))
* **deps:** update python:3-slim-bookworm docker digest to 0de8181 ([d5b0cf5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d5b0cf5824773967ada0848baab4342445c67f1c))
* **deps:** update python:3-slim-bookworm docker digest to 0de8181 ([2266805](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/22668058b2bf07a0ab323bc6a8d223e55d5b80a2))
* **deps:** update python:3-slim-bookworm docker digest to 1127090 ([47a3bbc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/47a3bbc8e9668e6eb7ba6332be2f898652910eff))
* **deps:** update python:3-slim-bookworm docker digest to 1127090 ([e47132b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e47132b535e139d73693336b57baa8984269881b))
* **deps:** update python:3-slim-bookworm docker digest to 23a81be ([61f2d4f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/61f2d4fdb1cb37c713d724be0b17273266c9a660))
* **deps:** update python:3-slim-bookworm docker digest to 23a81be ([27996a8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/27996a891fe99906073365fa1d3b074286f8ccd9))
* **deps:** update python:3-slim-bookworm docker digest to 2847b59 ([9641163](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/964116346aa5089ed39828bf8c81a0e3e29e6c60))
* **deps:** update python:3-slim-bookworm docker digest to 2847b59 ([a31757a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a31757a3460fd09e2ff88539671f8ab38d7e0246))
* **deps:** update python:3-slim-bookworm docker digest to a324f72 ([6b5e147](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6b5e14749e4c961333d01b906485179a211d6238))
* **deps:** update python:3-slim-bookworm docker digest to a324f72 ([33164ec](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/33164eccf9ffbab331195103f16fec24d64b76ef))
* **deps:** update python:3-slim-bookworm docker digest to a569688 ([3d72eca](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3d72eca25697d991cdb536fd7007dc9a7eba3b39))
* **deps:** update python:3-slim-bookworm docker digest to a569688 ([41b7c2d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/41b7c2d957ff81f4b927522dc3c3d1458ac49ba8))
* **deps:** update python:3-slim-bookworm docker digest to ae9f9ac ([095aa1e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/095aa1e1b6e487cf535a64539fd78add16197eca))
* **deps:** update python:3-slim-bookworm docker digest to ae9f9ac ([bc31584](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bc31584edf91d6c42222c14a2d825b20248f8c3a))
* **deps:** update python:3-slim-bookworm docker digest to b97f2df ([62c3424](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/62c342425d0e71cdb24f9b43afcd488f9e557f45))
* **deps:** update python:3-slim-bookworm docker digest to b97f2df ([5130abf](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5130abf1315d3d736c43066ffd00d6f2e6ade1f0))
* **deps:** update python:3-slim-bookworm docker digest to e4bf662 ([11f22e5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/11f22e5bb474d25452cb47eafd22e138f9fb526d))
* **deps:** update python:3-slim-bookworm docker digest to e4bf662 ([d7cad53](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d7cad53f8c0745e4380344b7d05a33a85c977f2d))
* **deps:** update python:3-slim-bookworm docker digest to f2d472e ([e7bb205](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e7bb2052f5821d1a61e7599f90c723236d9bb4b1))
* **deps:** update python:3-slim-bookworm docker digest to f2d472e ([67d8581](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/67d8581481fd30d7435a6a2154cdc98299ae9476))
* **deps:** update python:3-slim-bookworm docker digest to f3614d9 ([e87099c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e87099c086880a13315d2180c2d91e8bfbf53ff5))
* **deps:** update python:3-slim-bookworm docker digest to f3614d9 ([62d9fdb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/62d9fdbd67c13c84f2ec1dfd151d71d00894ea81))
* **deps:** update python:3-slim-bookworm docker digest to f41a75c ([68fe223](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/68fe223a1bddb8a415aadd5ca08e0e657b5012f1))
* **deps:** update python:3-slim-bookworm docker digest to f41a75c ([547a48e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/547a48ee7708e15957b3fa2534d2b37371612563))

## [2.5.1](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v2.5.0...v2.5.1) (2024-11-25)

### :bug: Fixes

* **protected-branches:** ensure idempotent when multiple allowed ([66878f9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/66878f9f9cd5ea2f4b7479615cba1be3828de6a3))
* **protected-branches:** ensure idempotent when multiple allowed ([cd8168b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cd8168b7a90476d26e8dbe76bb68571eb1cb9e21))
* **protected-branches:** restore CE compatiblity ([cbbb1d2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cbbb1d2b138965411658db085bcc55b749d5a288))
* **protected-branches:** restore CE compatiblity ([0163ce2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0163ce2f0b26bce3db1c9fc28e7aeca3143761e1))
* wait for protected branches when project is new ([51e32b3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/51e32b322b9b0b06ea7a8a9c9a61f6451fbdf5cb))

### :repeat: Chore

* **deps:** update alpine:3.20.3 docker digest to 1e42bbe ([afd6465](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/afd646551816e01cf7a6aae623bb1f6169589eaa))
* **deps:** update alpine:3.20.3 docker digest to 1e42bbe ([3ea23e5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3ea23e51c3fd79e1923611a434303524b2ab8ac8))
* **deps:** update debian:bookworm docker digest to 10901cc ([bb8af4c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bb8af4c879aa5548f7bca75ca36d76b6760eef8d))
* **deps:** update debian:bookworm docker digest to 10901cc ([835e403](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/835e4038b0b017e8c1c4a01bb957cfa9b91cd457))
* **deps:** update debian:bookworm-slim docker digest to ca3372c ([a451c82](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a451c826b8c8a879ac6d50c8f2249e41f825b2c7))
* **deps:** update debian:bookworm-slim docker digest to ca3372c ([605e1a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/605e1a30d21067ad4aa96a1faf0a9febe86123e9))
* **deps:** update debian:bullseye docker digest to 0155943 ([1812caf](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1812caffb3e0454a01d934bc4704146379997c7b))
* **deps:** update debian:bullseye docker digest to 0155943 ([a385d2a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a385d2a4eb32679c80db600409a6323baa1dde9d))
* **deps:** update dependency packaging to v24.2 ([bea5765](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bea57658098b080b46bda171c761cce0bdefc3a2))
* **deps:** update dependency packaging to v24.2 ([2f40167](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2f40167e17e2564b456f8964f313e505c2279683))
* **deps:** update dependency pygithub to v2.5.0 ([48a21d5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/48a21d50340ea48553660bfdea34d1f56da9fdcb))
* **deps:** update dependency pygithub to v2.5.0 ([d0a1c4e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d0a1c4e36464d2e6bdb6c35ee65b8c283bc55166))
* **deps:** update docker:27-dind docker digest to bec82cb ([39ebaf6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/39ebaf6f89e4a9fe5ea66baad362a9adfc30603c))
* **deps:** update docker:27-dind docker digest to bec82cb ([be30b45](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/be30b4560d06723f53ddae1709fc16d7d7f48f35))
* **deps:** update docker:27-dind docker digest to fa2054c ([27347d7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/27347d799ba26fd6c1af83c6471229d47eb68319))
* **deps:** update docker:27-dind docker digest to fa2054c ([3801fb5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3801fb571bd0b9831a094f9241ec4c7b2011ad8c))
* **deps:** update node.js to 5c76d05 ([6c7dc44](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6c7dc445beb506fcd3adb109782f5fa43fae7dfd))
* **deps:** update node.js to 5c76d05 ([9c80ee6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9c80ee64a8cb3b4e36a811abf220cb4bea540194))
* **deps:** update node.js to f496dba ([9836d87](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9836d877b08f99a7dbf5418479f90c1a13e83c75))
* **deps:** update node.js to f496dba ([a08773f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a08773f3da24deb294fb0e4db2b72aa4690ce846))
* **deps:** update python:3-slim-bookworm docker digest to 2d330ea ([b4cd36d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b4cd36dd31dabd6475d9c7bccbd06a44781d1d5a))
* **deps:** update python:3-slim-bookworm docker digest to 2d330ea ([f03b8ab](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f03b8ab1d47f6829492ffa042a2378f62fcf216e))
* **deps:** update python:3-slim-bookworm docker digest to 4efa69b ([6adcbbd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6adcbbd754b8e65ff91928db109868ee0a115969))
* **deps:** update python:3-slim-bookworm docker digest to 4efa69b ([99b44f0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/99b44f0fc860c84ede066cc35944f3f64ab3154a))
* **deps:** update python:3-slim-bookworm docker digest to ed304b4 ([08d0d71](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/08d0d71ef092f8a4e0972d3507a9d6f3e4aac99b))
* **deps:** update python:3-slim-bookworm docker digest to ed304b4 ([8c69b3b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8c69b3bf0ef467326f3192962cdcb907c6856d8b))

## [2.5.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v2.4.0...v2.5.0) (2024-11-08)

### :sparkles: Features

* **protected-branches:** sort output to ease reading diff ([628c9da](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/628c9da0a6a28bc04bd32c1255238af9f1522842))

### :bug: Fixes

* **protected-branches:** avoid diff with noone role ([8a3ff3d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8a3ff3de49f0648e5a67615a360f7740706fd88a))
* **protected-branches:** ignore access_level for user, group or deploy_key ([7606cd6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7606cd62f74181836215be198be1e4b3a9fb043c))
* various protected branches fixes ([14a79db](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/14a79db088067ef9d8bc2656fee6d3eab7e82c59))

## [2.4.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v2.3.0...v2.4.0) (2024-11-06)

### :sparkles: Features

* add more params to protected branches ([1c77605](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1c776057b4a38efcd2dfd45941eae18c57af9fb5))
* add more params to protected branches ([ac40162](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ac4016295e8f64e4becd72381fbdfdde21288991))
* push options ([9cade3b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9cade3b07f2e7b2b76be682ea6c5faf73665985f))
* push options ([b63dca6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b63dca68b96fb3ae35a3e91b9ddfce114ee51bd9))

### :bug: Fixes

* pygit2 http password mandatory when username ([7753052](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7753052a5ad89d242f4ebf2889fa56d32e622b8b))
* pygit2 http password mandatory when username ([3e711b0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3e711b01d3fc057e0f3a29eeb6d2f351b11ebbec))
* support protected branches on PyGitLab < 4.5.0 ([f6eaf58](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f6eaf581a5abc9a7452a5e6253c16bdc6b0b4d64))

### :zap: Refactor

* move protected branches to a mixin ([36d2b91](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/36d2b912efdf2116943d38066fef7d992a377fa6))
* move protected branches to a mixin ([a0bebc0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a0bebc0d2609de9c5597a7e0fc5df302f7bb1694))

### :repeat: CI

* ensure mixins inherit from GLraObject ([cbb35a8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cbb35a8c59606b97f37f495c8dc222da56125d4b))

### :repeat: Chore

* **deps:** libgit2 v1 transition was before bullseye ([b513e32](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b513e32669bd0b7e97b6c3eca8a9d40ceb8d375d))
* **deps:** libgit2 v1 transition was before bullseye ([5aff821](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5aff8211b2ab6d601282dd22e55b83d5a2d67dcc))
* **deps:** update debian:bookworm docker digest to e11072c ([bd55ee3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bd55ee370bb256d3dbfa06196e7d72b5af8f7c2b))
* **deps:** update debian:bookworm docker digest to e11072c ([94da607](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/94da607eed2bbdd04f365cd63455e2741d7cce82))
* **deps:** update debian:bookworm-slim docker digest to 36e591f ([f972b5c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f972b5cfebb030a559f8e39eba6300697b54a5c9))
* **deps:** update debian:bookworm-slim docker digest to 36e591f ([48e963b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/48e963bcbcad1a98ee27a1901d9863647339cf37))
* **deps:** update debian:bullseye docker digest to d0036be ([bff829b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bff829b272bbeaa9a2be9fe51163c89b1ce30846))
* **deps:** update debian:bullseye docker digest to d0036be ([0f3ba78](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0f3ba7883a51e7ba91840419389b2cf36d65af21))
* **deps:** update dependency flake8-comprehensions to v3.16.0 ([5b3643d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5b3643d6cdc0a4e60c78728ae6886733623cdcab))
* **deps:** update dependency flake8-comprehensions to v3.16.0 ([ad3f705](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ad3f7054655dfedec0be487f4e46d1a7c4d32905))
* **deps:** update dependency flake8-tidy-imports to v4.11.0 ([6ef7f26](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6ef7f26e6a6475abd50a762659d3ea9e8efc1483))
* **deps:** update dependency flake8-tidy-imports to v4.11.0 ([c13d6cd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c13d6cdd1d18e5efad8fdfcdfbcb93dafe9a861b))
* **deps:** update dependency mypy to v1.12.0 ([6b9e33b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6b9e33b8c0c8baf15630f452eb6f7c004d0e7d70))
* **deps:** update dependency mypy to v1.12.0 ([642e6d0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/642e6d00569d278d5b4107c66d0680c1443704b7))
* **deps:** update dependency mypy to v1.12.1 ([2387258](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/238725866d5bbed87f1ce819621019a14a90e109))
* **deps:** update dependency mypy to v1.12.1 ([ed68f8c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ed68f8c8d1c354d5535b17a16a647f26633ffbd6))
* **deps:** update dependency mypy to v1.13.0 ([2896c3a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2896c3a9911c031897dc71f7f8893a75ca590057))
* **deps:** update dependency mypy to v1.13.0 ([80fa8fd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/80fa8fda247cb288c4e9d5882514ed4b8a8a62c1))
* **deps:** update dependency pytest-cov to v6 ([6651f70](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6651f706f71efe643fbe8815e19cf3b257785b1d))
* **deps:** update dependency pytest-cov to v6 ([b48656a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b48656aaaede37a912d9bf8405f07657cc6bd855))
* **deps:** update dependency python-gitlab to v5 ([6ba054c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6ba054cf4dc0ff3e2c8b3cd1fe3a41627535dbc8))
* **deps:** update dependency python-gitlab to v5 ([ab0ee6f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ab0ee6f3431d766e52295e32cd09613c0256c5ab))
* **deps:** update dependency types-html5lib to v1.1.11.20241018 ([b3c14c2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b3c14c221fe0861724ccfdb0897fbaa58d12d7d3))
* **deps:** update dependency types-html5lib to v1.1.11.20241018 ([e3b4bda](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e3b4bdaa51f8f24316aeb115ae51193c30f78bd5))
* **deps:** update dependency types-requests to v2.32.0.20241016 ([d796fcc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d796fcc386f15b835dda866c17b98ca91c92dab1))
* **deps:** update dependency types-requests to v2.32.0.20241016 ([9d778ce](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9d778ce8edade6bae194edd24faa30e4ccea34c2))
* **deps:** update docker:27-dind docker digest to 25654f4 ([9264896](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/92648969f07b9c580f8a782fc91abf8cb13f7a06))
* **deps:** update docker:27-dind docker digest to 25654f4 ([8b6a304](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8b6a3041b88fb0c41ee0c0ace1e72041b23e2bcb))
* **deps:** update node.js to 8398ea1 ([ae749d7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ae749d7e4f2b2f29df13d47a38abe4e9b30e3743))
* **deps:** update node.js to 8398ea1 ([3c8185d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3c8185d9b80d9fcb59b7894ea31b3cc06b5bbc39))
* **deps:** update node.js to v22.10.0 ([fcd551c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fcd551c781c5af073e96023e55da47140832dc57))
* **deps:** update node.js to v22.10.0 ([2da3a29](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2da3a29365334e0e3bbfb36f4f6026fd2e263ede))
* **deps:** update node.js to v22.11.0 ([bc862d2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bc862d2f86ee50d44b451483fbf4be24bbc5de35))
* **deps:** update node.js to v22.11.0 ([d4d2283](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d4d22837559b778fad727935d4026e295f520c2e))
* **deps:** update python:3-slim-bookworm docker digest to 751d8be ([3f210a2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3f210a26beb8166172405c5df347c99d909415cf))
* **deps:** update python:3-slim-bookworm docker digest to 751d8be ([15dd90d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/15dd90d77123dd474f465de44a219f98eded0839))

## [2.3.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v2.2.0...v2.3.0) (2024-10-11)

### :sparkles: Features

* add GITLAB_TLS_VERIFY support ([f21783e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f21783e4f326d35c03feb9bafc30e25cf81685a2))
* add GITLAB_TLS_VERIFY support ([52198a8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/52198a87eec294ecc5daf54dce6f673549418ee5))
* add repo URL to docker image ([b8eca2d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b8eca2d33b4f61dfcd2352a7de2f7884af5f9da6))
* add repo URL to docker image ([ce9d280](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ce9d2807221d855e7e050eb0511fc3857238481e))
* allow to exit on warnings or errors ([25ede5b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/25ede5ba2ac8111a3a76e376d116b6b6d47ad9aa)), closes [#24](https://gitlab.com/gitlabracadabra/gitlabracadabra/issues/24)
* allow to exit on warnings or errors ([3e23346](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3e23346a380a7636a8e0204f697a2e63c2af6022)), closes [#24](https://gitlab.com/gitlabracadabra/gitlabracadabra/issues/24)
* map None values from API to empty string ([8e7ef90](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8e7ef900e9df7ef597e24a5460edff9ad9137a34))
* map None values from API to empty string ([e3880d5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e3880d5d34f39d597812c39635a0bc1bf8e90b67))
* more variables params ([e335a51](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e335a515de2541f424abb465719df9fca234400b))
* more variables params ([9ec9602](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9ec960249c4d92cb66016681b9c99f84c77f5d42))
* use uid in Dockerfile ([d121283](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d121283db08d9f25bbdeff394bc1cfce144b27d4))
* use uid in Dockerfile ([d641975](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d6419752d2d356a9f14b7fe5d89583aa06c5f041))
* variable value no longer mandatory ([ab14317](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ab14317b6df81106186988b37b853b104481e9de))
* variable value no longer mandatory ([ac9076c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ac9076c3324cf10e3c2ef5c39d9d58530fa65a64))

### :memo: Documentation

* document environment variables ([fc34408](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fc344080911c5fb8e72b5c1739ca415dbff71723))

### :repeat: Chore

* add builddep on setuptools ([b26d7dc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b26d7dce0a245282d5e5794ad422e67eee646c33))
* add builddep on setuptools ([1b3e2fe](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1b3e2fe5fdea3c3c5187aa79a89b82bbaf3ee50f))
* **config:** do NOT update urllib3 for python < 3.10 ([84f182c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/84f182c6826d0f4d344aac003ccae371ada915cd))
* **debian:** update Standards-Version to 4.7.0 ([36a6704](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/36a6704b2e546dd4a336d00ac50ca0e8244d0d9a))
* **deps:** pin urllib3 to 1.x for py<3.10 ([4ed1896](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4ed18960bd23bbacf2c6dd90215cb7f5a3d0c5ae))
* **deps:** update dependency build to v1.2.2.post1 ([f949f44](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f949f447a5613c0016133ac54f2a26680edb7fef))
* **deps:** update dependency build to v1.2.2.post1 ([d146e97](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d146e97e9034f36d715a644359178a1af8d8f07f))
* **deps:** update dependency python-gitlab to v4.13.0 ([0515fe6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0515fe668cbc3294dbbd71efc0220dea1e19b363))
* **deps:** update dependency python-gitlab to v4.13.0 ([7fec1f2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7fec1f28683fa4f11c76ccf10178fd779074202c))
* **deps:** update dependency urllib3 to v2.2.3 ([100cf03](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/100cf030096c1d1409eb318f178789584ec9477d))
* **deps:** update dependency urllib3 to v2.2.3 ([5fa7d7d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5fa7d7ddb271055a2d80af505db77fa7dd92a2e9))
* **deps:** update dependency vcrpy to v6.0.2 ([d81bd51](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d81bd5196b6e0479a6bc690c91c355a598d904c1))
* **deps:** update dependency vcrpy to v6.0.2 ([fce0e90](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fce0e9043e18fa876c31ce9756d81f4163563e91))
* **deps:** update python:3-slim-bookworm docker digest to 2ec5a4a ([951fa81](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/951fa81e60273f8c9aa6a3079680d87bd910402a))
* **deps:** update python:3-slim-bookworm docker digest to 2ec5a4a ([bdea5b2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bdea5b2a2beb5e2067044cd9a3470122cc43c9d4))
* **release:** dput is needed ([3222c2e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3222c2e5b194ccbd3806a98ae5b9226b8b641a64))
* **release:** fix Debian name ([d6ab688](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d6ab68847a2669b1b752c152c1613a34d076b82f))

## [2.2.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v2.1.0...v2.2.0) (2024-10-04)

### :sparkles: Features

* add comments in pypi requirements ([3dfcf00](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3dfcf005039891e37f79602ba8fd33b8e78688d9))
* add comments in pypi requirements ([f871402](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f87140223978218e55d7d40f8332a53ce2ccb244))
* add few of group parameters ([9c5dbfd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9c5dbfdbfe127cc0bb07be3ca3d337b11e2622e8))
* add few of group parameters ([392ab57](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/392ab57054585894edc9a426bdee82258e74bc2d))
* add force_random_password to user ([6803729](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/68037295fc12c61909f479c8cd01d02561dded16)), closes [#54](https://gitlab.com/gitlabracadabra/gitlabracadabra/issues/54)
* add force_random_password to user ([5dea2ad](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5dea2ade987deaf2065ed9d816b60f51f0e90bc1)), closes [#54](https://gitlab.com/gitlabracadabra/gitlabracadabra/issues/54)
* add lot of project parameters ([44e5a04](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/44e5a04b0afe611d560bc9d91b9c3d987777c53e))
* add lot of project parameters ([4a528ac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4a528ac685fb74754100936ec3e63696619db88e))

### :bug: Fixes

* newline mismatch for multi-line description ([7fe0091](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7fe0091109238e5b4e27b69a99560d81f90e2a43)), closes [#53](https://gitlab.com/gitlabracadabra/gitlabracadabra/issues/53)
* newline mismatch for multi-line description ([d2f9e8c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d2f9e8c873537a2f3f1107add0fd20a3903dbe1e)), closes [#53](https://gitlab.com/gitlabracadabra/gitlabracadabra/issues/53)
* typo in cron_timezone for variables ([b6095e3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b6095e3df2b9b8e4b61c628c50ed6b5a32416e66))
* typo in cron_timezone for variables ([79b6d8d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/79b6d8d5364fe42ace0b5907c49bdac05b73d527))

### :memo: Documentation

* fix markdown generation before release ([21faf31](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/21faf311ccac85a42fc0b8bd31af3b48797aeefa))

### :repeat: CI

* assert no errors before processing objects ([308c804](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/308c804a4376eba100c23387b3e0053a0fd93cb9))
* automergeDigest, enable major docker, and avoid Debian bump ([f18b28b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f18b28b4247a0aa448ae4acb8f202c18c4de53db))
* fix WPS306 Found explicit `object` base class ([41bdec1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/41bdec19d35e9dcfc711ef19edf406f7df16e4f5))
* fix WPS474 Found import object collision ([8231df4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8231df4136747b3661adccc97283d43897c472ba))
* handle different wordings ([c9fc40b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c9fc40b2e48d57c8b6515b8469f7744dc9b1e76c))
* pin alpine to 3.20.2 ([5bcf0c5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5bcf0c5ccaec6136666c4f3dd78666d748839764))
* remove License-Scanning job ([30c2882](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/30c2882855069378e84958fcabb9721f0717e550))
* remove License-Scanning job ([ea142c9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ea142c9769084bc7cb38e409720e1cbeb5a0e11c))
* remove now unused type: ignore ([9d688b8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9d688b87f3ea0cc216a409ea9f7d5addd501289b))
* skip some test until vcrpy is released ([b6dfe37](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b6dfe370999635e0fa3c3ef2bdbeca8346cf6e19))
* use latest images an pin digest ([f088dec](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f088dec16c2c8ee0830967081f13fcd86416ad75))
* use latest images an pin digest ([9059c3e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9059c3edae7070c6afe26709c0bed3258bae87ae))
* use renovate best practices ([ac2e6f7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ac2e6f7d3d2aea9ea0b32ba91f03c87339b53232))
* use urllib3 < 2 for Python < 3.10 ([ae2f33d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ae2f33d6087cc2109e9ab376aec3202f6d4ccc31))

### :repeat: Chore

* **deps:** pin dependencies ([d6a0919](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d6a09199cfe3c1827b0f3d64678a12b6f797cdc1))
* **deps:** pin dependencies ([4bc95ce](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4bc95cec0472980229b69ecd0100fa3f8ec1b9e0))
* **deps:** update alpine docker tag to v3.20.3 ([4edae72](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4edae7237b12896351e63dfbe8984abf71f3af63))
* **deps:** update alpine docker tag to v3.20.3 ([44dbf27](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/44dbf27b6a5c151055cfed0613d0d5b95929e1f2))
* **deps:** update debian:bookworm docker digest to 27586f4 ([b4e15e8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b4e15e809245e681abd8d6f6cb0e38ac98dc343f))
* **deps:** update debian:bookworm docker digest to 27586f4 ([5f1aba1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5f1aba1dd82dc138a17e40cf03826d457566025d))
* **deps:** update debian:bookworm docker digest to b8084b1 ([1665d62](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1665d62d1307164c110d846b68ee857251214e93))
* **deps:** update debian:bookworm docker digest to b8084b1 ([cf6ba41](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cf6ba41fdabb5b664547bc52e837281985ae0fd8))
* **deps:** update debian:bookworm-slim docker digest to a629e79 ([47d83e8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/47d83e85aedf0f545c5fee18726048c94b0c21f0))
* **deps:** update debian:bookworm-slim docker digest to a629e79 ([4b4de47](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4b4de47223d6c9cf704f06978f54051d16d579b5))
* **deps:** update debian:bookworm-slim docker digest to ad86386 ([bb6a345](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bb6a34530f0ab735356e3d42b7ffb43767105659))
* **deps:** update debian:bookworm-slim docker digest to ad86386 ([9163549](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/91635499e4d3c7a64776ce4c3af05339e14d273a))
* **deps:** update debian:bullseye docker digest to 152b9a5 ([7d0f055](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7d0f055f48b28ad54f549f6852e3c5d2ca6de93e))
* **deps:** update debian:bullseye docker digest to 152b9a5 ([2fe8be2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2fe8be2a4cda8eabbc695e166327a8824da8ee26))
* **deps:** update debian:bullseye docker digest to 8ccc486 ([a7a5384](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a7a5384831f72765ee9164f586a0e6be9e228332))
* **deps:** update debian:bullseye docker digest to 8ccc486 ([5dc09d8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5dc09d8fca1d1e848273541dfbfb1b2a04bbef9e))
* **deps:** update dependency bandit to v1.7.10 ([5e6dfa2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5e6dfa264afcb42bd41fc5e29e514a4a07814841))
* **deps:** update dependency bandit to v1.7.10 ([416518a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/416518af26fc9b2f372b38459aa2fc85113c3ca2))
* **deps:** update dependency bandit to v1.7.8 ([4e61541](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4e61541c71ad1189cd7d472972c614697a851a3f))
* **deps:** update dependency bandit to v1.7.8 ([8e124c8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8e124c83fc06e91afcea5adf20f3cde02cca2233))
* **deps:** update dependency bandit to v1.7.9 ([e791833](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e791833f3f4b9add2a81ed883e920583e3e6982b))
* **deps:** update dependency bandit to v1.7.9 ([a2b9bf6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a2b9bf6c7fec8bfd9591f839386fb1440efa1bb6))
* **deps:** update dependency build to v1.2.1 ([10ff87e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/10ff87ed59c984db4dbc1a9cdbf6cbff7d6794b9))
* **deps:** update dependency build to v1.2.1 ([a46e51e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a46e51e7d435c45d5cb31d2ae0dd01c314790a1b))
* **deps:** update dependency build to v1.2.2 ([acb838c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/acb838caf10b1da50299e63c464fdbe6de192461))
* **deps:** update dependency build to v1.2.2 ([96dc0ad](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/96dc0add4fdb8a647e02ef527b633e3b0b637b09))
* **deps:** update dependency flake8-builtins to v2.4.0 ([51fe786](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/51fe786ee2de178fe1079108b919e45922863c60))
* **deps:** update dependency flake8-builtins to v2.4.0 ([927ae0f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/927ae0f65220265509a499f46d596d575f3d5ca2))
* **deps:** update dependency flake8-builtins to v2.5.0 ([e021d0c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e021d0c6ac91d2bd0cddbb7bf553b8c4957966cb))
* **deps:** update dependency flake8-builtins to v2.5.0 ([d391d4b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d391d4b6775ccd2af9a67ce2471ec6f59525a306))
* **deps:** update dependency flake8-comprehensions to v3.15.0 ([74116c2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/74116c2c6d76c3037ea34739bd7887259528e774))
* **deps:** update dependency flake8-comprehensions to v3.15.0 ([54b6c7c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/54b6c7c6da20627564027c8e2d5b76c28a33fa03))
* **deps:** update dependency flake8-logging-format to v2024 ([c737dc4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c737dc492aeacb194dcd32f6e50441c52777c7e5))
* **deps:** update dependency flake8-logging-format to v2024 ([b390c9f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b390c9f01aae44cdfb4a888773757afab1090db3))
* **deps:** update dependency jsonschema to v4.22.0 ([19cfd31](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/19cfd31556e16a66059c14a05d8185e61560287d))
* **deps:** update dependency jsonschema to v4.22.0 ([0ca1d9a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0ca1d9a3ebb3e0e735a7a41b68b388c919dc3157))
* **deps:** update dependency jsonschema to v4.23.0 ([2bb8165](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2bb816548f202c7a10c3456ef7c30cf573ddcce2))
* **deps:** update dependency jsonschema to v4.23.0 ([21345fe](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/21345fe96c6bcae960031a945b8a2455d91776ca))
* **deps:** update dependency mypy to v1.10.0 ([9863d19](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9863d1968b0a3a29f5be9be75e18ce1e523cc4b3))
* **deps:** update dependency mypy to v1.10.0 ([4deb671](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4deb67126074e478ae988c63b932edadad1ba843))
* **deps:** update dependency mypy to v1.10.1 ([d8a0214](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d8a02145928bfb0125e6a0c3ef12b6a3d48001df))
* **deps:** update dependency mypy to v1.10.1 ([4b3be93](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4b3be93203a216d06e109ef8b6c766a5cdd7a656))
* **deps:** update dependency mypy to v1.11.0 ([13d6cf1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/13d6cf1cfa98a5bd2eb591cb173074736ad7b386))
* **deps:** update dependency mypy to v1.11.0 ([20d35ab](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/20d35ab9159410b09708f9601154c18e37624230))
* **deps:** update dependency mypy to v1.11.1 ([86d3555](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/86d35553173b94035905588acc8230064dc50cbd))
* **deps:** update dependency mypy to v1.11.1 ([c58d69f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c58d69f5f8b41236b6f438f5a013d7a14940b019))
* **deps:** update dependency mypy to v1.11.2 ([d04c6c0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d04c6c06f2f4af47ff3fd6a7b315fa4ce720a8bb))
* **deps:** update dependency mypy to v1.11.2 ([d735347](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d73534739ca29866f99f019b78458b59b413dbf1))
* **deps:** update dependency mypy to v1.9.0 ([98e49e1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/98e49e16f9875006a1e18a22ddca459e11a5d8ad))
* **deps:** update dependency mypy to v1.9.0 ([e61cfcc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e61cfcc3890f986709616dc7133f02640efd9304))
* **deps:** update dependency packaging to v24 ([800b219](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/800b219deb8753d9485a1ce9bbf9de53d554af05))
* **deps:** update dependency packaging to v24 ([f649ab2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f649ab27761fa3f26000e8610fa26ded53a5bbee))
* **deps:** update dependency packaging to v24.1 ([8ec033f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8ec033ff7e9c52271a2693d8555c884d185a692d))
* **deps:** update dependency packaging to v24.1 ([9271ca6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9271ca6df277f20e83d96a3b9830042542088075))
* **deps:** update dependency pygit2 to v1.15.0 ([da08c7f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/da08c7f62fc550e47597b1b4c2af0a937f8be6c9))
* **deps:** update dependency pygit2 to v1.15.0 ([6cf0624](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6cf0624967ac555d90ad996184db692714871b60))
* **deps:** update dependency pygit2 to v1.15.1 ([1e5c791](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1e5c791a703b282b4cc6ed9699f0b167646a939d))
* **deps:** update dependency pygit2 to v1.15.1 ([7913a9d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7913a9d8ed694d04acb191753cd1e5d76ec5c805))
* **deps:** update dependency pygithub to v2.3.0 ([231b3b2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/231b3b2b892351a55ba4730a22fc1ee6b8228a52))
* **deps:** update dependency pygithub to v2.3.0 ([b6f1194](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b6f1194a45fc751b87cdbe24fda616c19b95af66))
* **deps:** update dependency pygithub to v2.4.0 ([f685311](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f68531162c8fc6476639d93720d429f526739904))
* **deps:** update dependency pygithub to v2.4.0 ([8d66614](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8d666141125bfb64e8784de364209ba12e9ffac0))
* **deps:** update dependency pylint to v3.1.1 ([ea6bf99](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ea6bf99f45471b052645760f32e0bc48744d94db))
* **deps:** update dependency pylint to v3.1.1 ([b4bbf7b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b4bbf7b38578b535ebf4a9c520b51225ae104b07))
* **deps:** update dependency pylint to v3.2.2 ([ca269a5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ca269a55c8d7ed809d84937832e0339f2e618680))
* **deps:** update dependency pylint to v3.2.2 ([7d04290](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7d042901e544d95e84643bec4cb95921b4e00db2))
* **deps:** update dependency pylint to v3.2.3 ([e8e837d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e8e837db6c6254c1a0eb174c1b747079320301f9))
* **deps:** update dependency pylint to v3.2.3 ([8dba1ed](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8dba1ed3c1e6d1c59afc6ac4ac5c86cfed465a62))
* **deps:** update dependency pylint to v3.2.5 ([a7f5ae5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a7f5ae58b41912c33194d5b8abf8180e1ca6e2ae))
* **deps:** update dependency pylint to v3.2.5 ([7d2ec2e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7d2ec2e14aa132ed9264e5b1854c9237ab408adf))
* **deps:** update dependency pylint to v3.2.6 ([a5e74b5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a5e74b5ff3703226e1f90b2f95a07f75ec7fd180))
* **deps:** update dependency pylint to v3.2.6 ([2f485bc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2f485bc2d090050a29f8c21542de836e9cfb67fc))
* **deps:** update dependency pylint to v3.2.7 ([583b94d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/583b94d9d67a63be627c09eca7720432277b3fa8))
* **deps:** update dependency pylint to v3.2.7 ([c7ce2a0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c7ce2a00db5f48c1d85ababe2fd2f04d85ec035b))
* **deps:** update dependency pylint to v3.3.0 ([9e95247](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9e9524753f28f87ecfe66545112c7aeff5a8a91a))
* **deps:** update dependency pylint to v3.3.0 ([2b7aafc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2b7aafcd7fbab2667cb3b39a069fa31e83281ac8))
* **deps:** update dependency pylint to v3.3.1 ([1fbd33e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1fbd33e4a6df9e4f35f6ce7758238c4821a61c47))
* **deps:** update dependency pylint to v3.3.1 ([d6c93f8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d6c93f81e8d5fbbd0e7677f5a4fe98433c7adaa2))
* **deps:** update dependency pytest to v8.1.1 ([88e959e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/88e959ed432f305dc4bc270b0e2f89dba386c45e))
* **deps:** update dependency pytest to v8.1.1 ([c26d51f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c26d51f9047d3eb47b725409e1db2420586c3ef4))
* **deps:** update dependency pytest to v8.2.0 ([8030a29](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8030a29386fa76b2a81b1882aa440490808b12e2))
* **deps:** update dependency pytest to v8.2.0 ([7fcabe7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7fcabe7a1726ccaeccdff3ffcd4432be51d8d9da))
* **deps:** update dependency pytest to v8.2.1 ([2bfdefe](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2bfdefe8a100d967cef5c3aa73099f66895373bb))
* **deps:** update dependency pytest to v8.2.1 ([d603a71](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d603a715eb6a6ce2beee0e2f08ced94e22cef066))
* **deps:** update dependency pytest to v8.2.2 ([315d2bc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/315d2bcf38948054431be01fc7c1ec31141bcd75))
* **deps:** update dependency pytest to v8.2.2 ([70c9cd3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/70c9cd3cadd029a29b3fc75b48bd3772fce9e6d8))
* **deps:** update dependency pytest to v8.3.1 ([524298a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/524298a9291fcd5ac756567a8b59726d70be8c96))
* **deps:** update dependency pytest to v8.3.1 ([62f12a4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/62f12a4f56a191f40e56349259dbef369b888c6a))
* **deps:** update dependency pytest to v8.3.2 ([ad68cf8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ad68cf89aeaf755da6da70a071ce085f4ab4a5e1))
* **deps:** update dependency pytest to v8.3.2 ([f8162ae](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f8162ae1d0fdf2dd0dff617ccd32552ce1d3b0a3))
* **deps:** update dependency pytest to v8.3.3 ([5a13376](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5a13376e12d129aa637321978275c340eed482b8))
* **deps:** update dependency pytest to v8.3.3 ([5e2fb1c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5e2fb1cc6c9d2867b34a5fc64a1a049da4cb88bd))
* **deps:** update dependency pytest-cov to v5 ([bd601e6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bd601e63f7449e025baefa6fea9ca604872349bf))
* **deps:** update dependency pytest-cov to v5 ([53d3619](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/53d36197d30a45b3e717a827de3180ba54cd5fd9))
* **deps:** update dependency python-gitlab to v4.10.0 ([576bc66](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/576bc6642ba693b0f188e3bb48a51c760ca65bab))
* **deps:** update dependency python-gitlab to v4.10.0 ([18de75b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/18de75b8e1dd27b2a5d2de0d77d29ace4a7ecb00))
* **deps:** update dependency python-gitlab to v4.11.1 ([9a0f87c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9a0f87c15e60eb160f9be0aaeaabda99043463b3))
* **deps:** update dependency python-gitlab to v4.11.1 ([d42e5f6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d42e5f6b4b548fdae2622a73a18a213b52ffc280))
* **deps:** update dependency python-gitlab to v4.12.1 ([1e50003](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1e500036f9ca4cfdd1bca1338f800154a4d6df6f))
* **deps:** update dependency python-gitlab to v4.12.1 ([5ac4434](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5ac4434c079ab7462aec46a07746e437bd656694))
* **deps:** update dependency python-gitlab to v4.12.2 ([e9fdfbf](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e9fdfbf065a4693d1550abb5f16fc17d364408bb))
* **deps:** update dependency python-gitlab to v4.12.2 ([067fad7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/067fad7c62fb8095581edf6383663b066aaec921))
* **deps:** update dependency python-gitlab to v4.5.0 ([275f38f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/275f38ffc9adb90d511f4c78069a6bbfd827224d))
* **deps:** update dependency python-gitlab to v4.5.0 ([4e62bce](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4e62bce74123a14a65f6af072145da10bb59d926))
* **deps:** update dependency python-gitlab to v4.6.0 ([9afc88e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9afc88e2cbc27e7730376b13d9855d60e58e3bd0))
* **deps:** update dependency python-gitlab to v4.6.0 ([f770670](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f7706701b74f8fc7048c49b91cf7befa6ddd8c22))
* **deps:** update dependency python-gitlab to v4.7.0 ([50228e5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/50228e58944b84a0046c610d47254587ccef4491))
* **deps:** update dependency python-gitlab to v4.7.0 ([9575c79](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9575c796b515f3df384ce0e1aba24609f3b12e5f))
* **deps:** update dependency python-gitlab to v4.8.0 ([f172bab](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f172bab1936febc2be08821803e2dc7d75d08c99))
* **deps:** update dependency python-gitlab to v4.8.0 ([dd67883](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dd678838a7996eafe88f18789c2c51a02aa96f13))
* **deps:** update dependency python-gitlab to v4.9.0 ([9854549](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9854549aa71b2a0ae45ebb2af8558461d3bcf910))
* **deps:** update dependency python-gitlab to v4.9.0 ([e80616e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e80616e80d6943230ec1e809120602b539d3de68))
* **deps:** update dependency pyyaml to v6.0.2 ([ce3c36e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ce3c36edf30f44d12c8f61a5e64d7efaad603fff))
* **deps:** update dependency pyyaml to v6.0.2 ([1f10e2d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1f10e2d879e27bd40dfdfb57159492d87966ac69))
* **deps:** update dependency types-html5lib to v1.1.11.20240806 ([79888f1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/79888f1a85417585a32f9632a62d819bb1469bbf))
* **deps:** update dependency types-html5lib to v1.1.11.20240806 ([4597d5c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4597d5cd4e4ccea55fc002bd61cb3426a05cfa09))
* **deps:** update dependency types-pyyaml to v6.0.12.20240311 ([ac3cfc6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ac3cfc617f95aa115392b93292ff57b01f975b22))
* **deps:** update dependency types-pyyaml to v6.0.12.20240311 ([92c3c4b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/92c3c4b87b19e77f052bffd854ff8d44e6ef3175))
* **deps:** update dependency types-pyyaml to v6.0.12.20240724 ([155f5cb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/155f5cbd2048fa20c04073899d04649329772afa))
* **deps:** update dependency types-pyyaml to v6.0.12.20240724 ([a01a0df](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a01a0df94231c10d495c097a25f1db99b3378fcf))
* **deps:** update dependency types-pyyaml to v6.0.12.20240808 ([ed41fbc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ed41fbcc162c622e67cfc10f4f1d6a33f0b703b4))
* **deps:** update dependency types-pyyaml to v6.0.12.20240808 ([7380a76](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7380a76384cfcf1ff65befc97b922b7061df933a))
* **deps:** update dependency types-pyyaml to v6.0.12.20240917 ([76faa6f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/76faa6f4c1e6889a1305261d3fa4a108c2e44a03))
* **deps:** update dependency types-pyyaml to v6.0.12.20240917 ([8348281](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/83482816396ae83c47f696b8af1de39c3c100e12))
* **deps:** update dependency types-requests to v2.31.0.20240311 ([c19bdcc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c19bdcc8524fc8c6490881ce93e648f7944ee718))
* **deps:** update dependency types-requests to v2.31.0.20240311 ([d3f8b7d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d3f8b7d0d6af3d16111274490011d925c5e4cfb0))
* **deps:** update dependency types-requests to v2.31.0.20240402 ([1645877](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/16458778f51f6b030ee744b1fa7b9fab68892f72))
* **deps:** update dependency types-requests to v2.31.0.20240402 ([3111f29](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3111f29d452757aac0748ae8c19d978e2bb854c8))
* **deps:** update dependency types-requests to v2.31.0.20240406 ([ce45dd3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ce45dd38bae4b33118c444b264c2c15098f1bd1a))
* **deps:** update dependency types-requests to v2.31.0.20240406 ([afcdf6c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/afcdf6cc60cbb3afc001c4fc7301d0f89a90d6fa))
* **deps:** update dependency types-requests to v2.32.0.20240602 ([be9ed32](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/be9ed32b3bc862f01d68d1633af0ea0d109f2ad5))
* **deps:** update dependency types-requests to v2.32.0.20240602 ([ddb5bcc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ddb5bcc3ca3308eedde0aa96d58b99752875d341))
* **deps:** update dependency types-requests to v2.32.0.20240622 ([2de29b4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2de29b447ee8be2146fbc9a952e1cd83e872b13e))
* **deps:** update dependency types-requests to v2.32.0.20240622 ([c4d73db](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c4d73db7d1f405eceea789e6b7a4d3e5975c9fbb))
* **deps:** update dependency types-requests to v2.32.0.20240712 ([2e377a6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2e377a6f8d2f38b649e9f83698997ed20a6fdd3a))
* **deps:** update dependency types-requests to v2.32.0.20240712 ([26c6059](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/26c6059d033f04668cd5deb916388a7981f8d66d))
* **deps:** update dependency types-requests to v2.32.0.20240907 ([ee5d5ca](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ee5d5ca01bcefad86bbdad9eb74ba92af4d31123))
* **deps:** update dependency types-requests to v2.32.0.20240907 ([d2d6471](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d2d6471651976d036baecdf6edabfbf947a8f0f5))
* **deps:** update dependency types-requests to v2.32.0.20240914 ([cc3cc31](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cc3cc31cb77b22f901aeeed2773e049694f8f1e0))
* **deps:** update dependency types-requests to v2.32.0.20240914 ([bca7ded](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bca7dedf3ac96df3b83672730d34d91d9a00fa77))
* **deps:** update dependency urllib3 to v1.26.19 ([1eefa5c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1eefa5c9b0dfff509517ef9d5e31a504e7df120d))
* **deps:** update dependency urllib3 to v1.26.19 ([7acbb73](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7acbb73099a33b55bda2ef3eafd86bc82dbfff49))
* **deps:** update dependency urllib3 to v1.26.20 ([72df78f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/72df78f52dfc1ae91a3b81716ead3bd544205eb6))
* **deps:** update dependency urllib3 to v1.26.20 ([20d9f33](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/20d9f338cdf1fd7b79526669ea87d28bb11398e5))
* **deps:** update dependency urllib3 to v2 ([52c5e2e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/52c5e2e512429b2f8c7ac2ba89e9c73fe8fbc418))
* **deps:** update dependency urllib3 to v2 ([920a28a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/920a28aed35d75be8b9d0d80fbb69bf54c1da74f))
* **deps:** update dependency wemake-python-styleguide to v0.19.2 ([4c3e6bc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4c3e6bcb328f4da602bc0e9a93e0dae559a176a6))
* **deps:** update dependency wemake-python-styleguide to v0.19.2 ([6f3ab6c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6f3ab6c46350d203c0c20d49f76b2bf94fe62e65))
* **deps:** update docker docker tag to v26 ([3a4d6ac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3a4d6acda7699fe1e581f0528279a516bd4ea435))
* **deps:** update docker docker tag to v26 ([e93c7c8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e93c7c81734a3fc9abf7734f8fc7b4c89d691f2c))
* **deps:** update docker docker tag to v27 ([4833314](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4833314c1ca0a01acc5bf4fbca33cbc9f8c42183))
* **deps:** update docker docker tag to v27 ([5786f63](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5786f634a414e27f1ecfd1d013e361b430327685))
* **deps:** update docker:27-dind docker digest to 8d50398 ([d0c7a77](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d0c7a773873202d7f44dc2ac1bdaba63c5f8f6ed))
* **deps:** update docker:27-dind docker digest to 8d50398 ([e1b7cac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e1b7cac71cb9f5c950df1373a2ac927ae57898cf))
* **deps:** update docker:27-dind docker digest to 9cb900a ([5fa7b12](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5fa7b12c72641d17ebae18593cb5753807ee2b57))
* **deps:** update docker:27-dind docker digest to 9cb900a ([9428311](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/942831181205fda5eb09333380e30c2bd31cd072))
* **deps:** update docker:27-dind docker digest to c51fa20 ([2c72bb1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2c72bb112d0bcbc83e03ade305125960314221e5))
* **deps:** update docker:27-dind docker digest to c51fa20 ([89ebf5c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/89ebf5caf90bcf525de7958f6bda6e3f2defdeaa))
* **deps:** update docker:27-dind docker digest to ecfcb83 ([e2d0365](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e2d03656e5a67493077f29e95a19f08197ffc48b))
* **deps:** update docker:27-dind docker digest to ecfcb83 ([ab7efe1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ab7efe1c44c437a12aa030b1cb149477ccf3d44f))
* **deps:** update hadolint/hadolint:v2.12.1-beta-alpine docker digest to e6f3fb9 ([424de86](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/424de86c23d21145bd13c24b692edd255d45c5de))
* **deps:** update hadolint/hadolint:v2.12.1-beta-alpine docker digest to e6f3fb9 ([277da21](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/277da219c343be15b3a82e6be1ff21e9c1153b43))
* **deps:** update node.js to 69e667a ([8be2ca1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8be2ca154c0f9ae0b176f9b32b1fa7d745fab669))
* **deps:** update node.js to 69e667a ([c5fcfce](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c5fcfcef51aada64d537b5e2a50a9a2b69fccec3))
* **deps:** update node.js to v21.7.1 ([bbc0218](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bbc0218dbfb640be19c0bcbbab5e0980e21c954c))
* **deps:** update node.js to v21.7.1 ([960feb2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/960feb2c9f5a2c46d6fb4a4ccb1e42559a908fe5))
* **deps:** update node.js to v21.7.2 ([66be0d3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/66be0d3365db525849212d007baae48bf3aaf46d))
* **deps:** update node.js to v21.7.2 ([47c1b1f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/47c1b1f5ffb85de338fb22588c9fab3dcc3a0a91))
* **deps:** update node.js to v21.7.3 ([dad0ef8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dad0ef8f37bb03fb084151fc672ce431e47822d1))
* **deps:** update node.js to v21.7.3 ([4afa460](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4afa46061aaa1adbacf19ce1ac0870bad1237314))
* **deps:** update node.js to v22 ([bd9cf6b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bd9cf6bc9f398dc095cbf4e461b54d1c1c3721be))
* **deps:** update node.js to v22 ([3cbb003](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3cbb003402943650f6d6563c72463d813f56f1c9))
* **deps:** update node.js to v22.2.0 ([4ec9dac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4ec9dac84188e6b34bdccbf46d4cf5573bd69e23))
* **deps:** update node.js to v22.2.0 ([c6d3d61](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c6d3d616438a8cb558571dc435b4f1455a0f990e))
* **deps:** update node.js to v22.3.0 ([0672a30](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0672a3071cf9a14a0d67e43ab3855410a24f7c25))
* **deps:** update node.js to v22.3.0 ([6d1c3f1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6d1c3f1ba9a5ee77f10bbd9153e1a7a3a2f46e62))
* **deps:** update node.js to v22.4.0 ([abbb13b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/abbb13be34c2f9bf1cb061579a71b84fee6ed0a0))
* **deps:** update node.js to v22.4.0 ([adae0c4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/adae0c4eda555e8f76bd16232afd1745266a71c3))
* **deps:** update node.js to v22.4.1 ([f6df8cd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f6df8cdc3973de9d22b95590a47a3789dfc4e6f1))
* **deps:** update node.js to v22.4.1 ([df79570](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/df795709836cf54402796219fc865135e14c5698))
* **deps:** update node.js to v22.5.1 ([b71ab4c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b71ab4cb507075edf87f632896276e84fac7ed0b))
* **deps:** update node.js to v22.5.1 ([e407646](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e4076468bb2308752472b0fb54c54e70f1c82901))
* **deps:** update node.js to v22.6.0 ([c8d2c48](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c8d2c481ec1ec97585aaeee20da93cea99602d12))
* **deps:** update node.js to v22.6.0 ([1f4e7a5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1f4e7a520560557aec95ad6eb6fec6d455be192c))
* **deps:** update node.js to v22.7.0 ([00a47c6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/00a47c62144ace48050f7cd8f8750861639dbd32))
* **deps:** update node.js to v22.7.0 ([e136c8e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e136c8eafeeccecae2556008b0a7085822c077d6))
* **deps:** update node.js to v22.8.0 ([0211b41](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0211b411a8a8c4a091b33a1e2e7e452cc13f3863))
* **deps:** update node.js to v22.8.0 ([077f71b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/077f71ba9791e6d182b897489c8ec30efda07aa7))
* **deps:** update node.js to v22.9.0 ([a976e03](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a976e038f0dd65bdf9625f9880625161d36bd1c4))
* **deps:** update node.js to v22.9.0 ([e3df89d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e3df89de1ff2633ac6b0ee932967451d422c3032))
* **deps:** update python:3-slim-bookworm docker digest to 15bad98 ([d91738f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d91738f7ff9e59ef5b76b27f5f200026ab31a5db))
* **deps:** update python:3-slim-bookworm docker digest to 15bad98 ([13065c9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/13065c99fb6531a2098610d74c6cd01098677649))
* **deps:** update python:3-slim-bookworm docker digest to 8ac54da ([ea0b094](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ea0b094579c31da3808b2ad3db7dc2c9b2b08325))
* **deps:** update python:3-slim-bookworm docker digest to 8ac54da ([1fae75f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1fae75f1061b5208d3410e917e0bee64b415252e))
* **deps:** update python:3-slim-bookworm docker digest to ad48727 ([b5ef1bb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b5ef1bb8e3be3425d1ba4a17967beef103d8b7f1))
* **deps:** update python:3-slim-bookworm docker digest to ad48727 ([24727ba](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/24727bacb8c039a564cfa2e9c9b3ff69c4ecb44e))
* **deps:** update python:3-slim-bookworm docker digest to af4e85f ([e10930a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e10930a8c65b3d378d763a5ea2200665be5f3816))
* **deps:** update python:3-slim-bookworm docker digest to af4e85f ([e588367](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e58836714b2f383140b5cbf8c204fe86a84f6ced))

## [2.1.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v2.0.0...v2.1.0) (2024-3-6)


### :sparkles: Features

* allow to use env vars for PyGitLab ([6cf49e1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6cf49e1aa013d6966ddbf0d58a42c08df6d636d1))
* allow to use env vars for PyGitLab ([baf4961](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/baf4961087bb68cf55cf4ad0fa305ef324f5cf3a))


### :memo: Documentation

* update release docs ([99f654d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/99f654df22df80c823763c43f039f65eb6538d81))
* update release docs ([a0ffaec](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a0ffaec26a931af16b6acd7e8f15a29d72d42a6f))


### :repeat: Chore

* **deps:** update dependency types-html5lib to v1.1.11.20240228 ([20e4922](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/20e4922a73af585dab14ac8aea668f7ff2375a61))
* **deps:** update dependency types-html5lib to v1.1.11.20240228 ([03e9b28](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/03e9b28ea8f1b87609a5eadea925205c7ab1add0))

## [2.0.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.13.0...v2.0.0) (2024-3-6)


### ⚠ BREAKING CHANGES

* minimum supported Debian version is now bullseye

### :sparkles: Features

* **build:** stop invoking setup.py directly ([7fbc10f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7fbc10f60d41dd05222824de475d23aa855b70ea))
* **build:** stop invoking setup.py directly ([cc1b74e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cc1b74ed82581acf44132e8ae5d0133efa3eb319))


### :bug: Fixes

* **containers:** handle HTTPError without response ([521d6cb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/521d6cb3d74117d1e7c1e112b7120c8e9c0f6209))
* **github:** ensure we always output full exception ([ba0645b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ba0645bc4b5ee436ab891016712a318326de31b4))
* **pep8:** fix E721  do not compare types ([174b95c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/174b95c977690816396571afc1030faca72d8b78))
* **pep8:** fix E721  do not compare types ([f903eeb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f903eebfe8249a30d128bbd8352edcf37f0e4465))


### :memo: Documentation

* update release docs ([2858571](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/28585716270ab62b68730ecb6c4fafc5e444ccde))
* update release docs ([1314eef](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1314eefcc78c678d3703e8769a30dfe4df78a301))


### :repeat: CI

* err.request can be None ([3b2d8e2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3b2d8e263602c16190059e3ca62eec8de8ea8c01))
* **flake8:** no more accepting comments in ignores ([d8a48d5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d8a48d5b097215c47fd61a143512223eb73dece2))
* **pep8,mypy:** always use base requirements ([4ec797c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4ec797cdd882cc6e5694d0aee3a88d4faf3f4df3))
* **pep8,mypy:** always use base requirements ([9871b69](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9871b694eb3b77210ba48cba8b6b597864ebb6e1))
* remove blank line ([e8a933f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e8a933fb096090c62c66582ebeb5405bea1675e4))
* update to latest AutoDevOps template ([abc57d7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/abc57d7974fa77894c98edc4ed674b73541343ad))
* use newer dind image ([3811832](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3811832f8945811bfec717b21c71094d849af4f8))


### :repeat: Chore

* **deps:** update dependency bandit to v1.7.6 ([ec2a72d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ec2a72d529068e482af7ad60ba2715531796033f))
* **deps:** update dependency bandit to v1.7.6 ([156dfbe](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/156dfbeb571ba0f3c285a8f8bb1e79f842ca8025))
* **deps:** update dependency bandit to v1.7.7 ([77cf6ac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/77cf6ac6d6ac5fb8e55e6dbd12f7304c83608c03))
* **deps:** update dependency bandit to v1.7.7 ([2fce5f8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2fce5f8b1d395e262c4ac23ebe16ef493a42a477))
* **deps:** update dependency build to v1 ([6ed2c6a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6ed2c6afccef1415877d7e37502ca5750a7ef7c7))
* **deps:** update dependency build to v1 ([583d238](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/583d2386e32701ba63db35639b323d564f1e602d))
* **deps:** update dependency build to v1.0.3 ([0535680](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/05356801a7a941effd9427bddb763f0dc2accdea))
* **deps:** update dependency build to v1.0.3 ([c11a70f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c11a70f7fd484fb63e061ad3740531ec321f74cf))
* **deps:** update dependency build to v1.1.1 ([408c051](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/408c051ce3cbc83879362e134dea130d2101faee))
* **deps:** update dependency build to v1.1.1 ([172c22d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/172c22d1ac67dc898dda1a840cedf0d208075065))
* **deps:** update dependency flake8-builtins to v2.2.0 ([fd368c7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fd368c7620147c1ec87d4991336a6f135e5ceeab))
* **deps:** update dependency flake8-builtins to v2.2.0 ([2850e12](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2850e125c52e475ab132dfa0271b62e1a58f8664))
* **deps:** update dependency flake8-comprehensions to v3.13.0 ([8ccb4dc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8ccb4dc0b49aaf51512e72027b39c0f9a6226ce9))
* **deps:** update dependency flake8-comprehensions to v3.13.0 ([21f31ac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/21f31ac7b4a5d5d61113150f2ac8022c18191aa1))
* **deps:** update dependency flake8-comprehensions to v3.14.0 ([2d4ad86](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2d4ad86493ffe8f04b16a6731184f655e8967c92))
* **deps:** update dependency flake8-comprehensions to v3.14.0 ([d76c6a9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d76c6a96848d8de9aa4cb561d0eb240a394efdad))
* **deps:** update dependency flake8-deprecated to v2.1.0 ([5db6ac8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5db6ac86cf9d87ab60dbd1004f7da1acdfc774f7))
* **deps:** update dependency flake8-deprecated to v2.1.0 ([8f076e6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8f076e61552c740b19f0d3b8b68171020af22f21))
* **deps:** update dependency flake8-deprecated to v2.2.1 ([fe6f6a1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fe6f6a124a0b260fc0f0f403d00c19dbb0f89f87))
* **deps:** update dependency flake8-deprecated to v2.2.1 ([06e9e5f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/06e9e5fd5e54e71a5521398efc890e745d7b8179))
* **deps:** update dependency flake8-quotes to v3.4.0 ([422c7f0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/422c7f00d17c9bb33b8b45713e2541d9523fb982))
* **deps:** update dependency flake8-quotes to v3.4.0 ([c471c01](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c471c01874382fd5a24e919a9743acd4f116d605))
* **deps:** update dependency flake8-tidy-imports to v4.10.0 ([028a59a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/028a59a63b06c0b90f428a07d25719f9d3eae198))
* **deps:** update dependency flake8-tidy-imports to v4.10.0 ([9fe3b31](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9fe3b31f95ac444b399f536534124fb417675088))
* **deps:** update dependency flake8-tidy-imports to v4.9.0 ([aaf3ea9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/aaf3ea9e17810554e15841173210d3d1b6fbc1d4))
* **deps:** update dependency flake8-tidy-imports to v4.9.0 ([9a5d25f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9a5d25f1e87b7070219874d0cf3cd9447fee4905))
* **deps:** update dependency isort to v5.12.0 ([7993343](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/799334395af3b88349f82d74082ffec0fe7692e9))
* **deps:** update dependency isort to v5.12.0 ([3b9c93f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3b9c93f80efd1970667a45941f015a0da4e5de97))
* **deps:** update dependency isort to v5.13.1 ([cfadef9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cfadef9b7b5b7e90a4601d3b12ec984bf916fc56))
* **deps:** update dependency isort to v5.13.1 ([f427061](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f427061b427fd07c6227aae5907c3102399300cb))
* **deps:** update dependency isort to v5.13.2 ([946a90a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/946a90ad187e3b68fd8193751ab1e7b6f17a9133))
* **deps:** update dependency isort to v5.13.2 ([ff04925](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ff0492594a3f8ff669aa6f7174a2f5ea13288513))
* **deps:** update dependency jsonschema to v4.18.0 ([202e167](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/202e1679b2ce6ecdff43845216b70c3ff18303df))
* **deps:** update dependency jsonschema to v4.18.0 ([fa83603](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fa83603de8162a63c5dd91b2229849f5b08bb5f5))
* **deps:** update dependency jsonschema to v4.18.3 ([354e579](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/354e5794cc4eb6fcb67b1ceb640bd821311f3f4b))
* **deps:** update dependency jsonschema to v4.18.3 ([e2657c5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e2657c5485dcc1414aea3beb88f1547cda0eee49))
* **deps:** update dependency jsonschema to v4.18.4 ([87b5b01](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/87b5b01f92e5c6a147aef7d697660bbda7e484c4))
* **deps:** update dependency jsonschema to v4.18.4 ([eed76e7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eed76e736640df59719b5ef603d98297ee7f887e))
* **deps:** update dependency jsonschema to v4.19.0 ([b094d1f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b094d1f681e1ccda8af071c579bd0c53ff185655))
* **deps:** update dependency jsonschema to v4.19.0 ([e2fbb8a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e2fbb8a36b4d31683b3d561e704eb2f8baafd932))
* **deps:** update dependency jsonschema to v4.19.1 ([7546a16](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7546a16a1db157fb2d21a8cf7b6ddf674d55f7b0))
* **deps:** update dependency jsonschema to v4.19.1 ([6a0bd4b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6a0bd4b4bdf5f3f300a7d040db3a4bd7919367f9))
* **deps:** update dependency jsonschema to v4.19.2 ([7fbd382](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7fbd38235a152b1beb0fd635161b7ba7a5d282db))
* **deps:** update dependency jsonschema to v4.19.2 ([cbba18e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cbba18e423c7d7a9c2a06c2dc09534a7501c2c7e))
* **deps:** update dependency jsonschema to v4.20.0 ([4d714a8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4d714a8d42c48d7ec5b6fe1332a9f8ea9a3de9ad))
* **deps:** update dependency jsonschema to v4.20.0 ([1a8c1b6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1a8c1b673d86422918b001e945b8c6ba86584f1c))
* **deps:** update dependency jsonschema to v4.21.1 ([9e4a83b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9e4a83b2ab8c0230c2c418170e74ace40b42e8ec))
* **deps:** update dependency jsonschema to v4.21.1 ([d638292](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d6382925438cfab9127c9e69362192d8b07b61ee))
* **deps:** update dependency mypy to v1.4.1 ([e29e95d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e29e95d5aeebf766189f6a89b6c7907a769655af))
* **deps:** update dependency mypy to v1.4.1 ([2434251](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2434251a1af990a673b96a17a0ef52982b70c49e))
* **deps:** update dependency mypy to v1.5.0 ([772e5fd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/772e5fd5153bbea8e2ba78bce9aeebd2e13edaad))
* **deps:** update dependency mypy to v1.5.0 ([484ba13](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/484ba13b065d38cf8929c1cdade2b64b557f81c4))
* **deps:** update dependency mypy to v1.5.1 ([a6f6c6a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a6f6c6a0d870f5b583cd6168b206b3c9f6285df0))
* **deps:** update dependency mypy to v1.5.1 ([b84f83b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b84f83be7d7ce8188ba2ad480043dc592d538d06))
* **deps:** update dependency mypy to v1.6.0 ([07bb09d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/07bb09d892354e9cdb44f49e033d50b383ca1878))
* **deps:** update dependency mypy to v1.6.0 ([6000468](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/60004681aa7bb27239bb52c38929296a186264ba))
* **deps:** update dependency mypy to v1.6.1 ([35265cc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/35265ccd7b2a9d57b9e184bed8d2b021b9905205))
* **deps:** update dependency mypy to v1.6.1 ([59de11f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/59de11f221fa9738a03d84ff45100e92bce5a136))
* **deps:** update dependency mypy to v1.7.0 ([71d2d0b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/71d2d0b15be0a9de33bde70850d01fb24814e902))
* **deps:** update dependency mypy to v1.7.0 ([9eb294a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9eb294ae7f51d1119a981f5f534007f93c366bf1))
* **deps:** update dependency mypy to v1.7.1 ([784c5cf](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/784c5cf6909eed2364bf01d4eb8476cd8a0b71a0))
* **deps:** update dependency mypy to v1.7.1 ([0a7afe7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0a7afe71a45fa00239de1d053f5b9bc3ca150097))
* **deps:** update dependency mypy to v1.8.0 ([b97fe0e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b97fe0e648bc84852b4ed73ecca036b4a5c2917a))
* **deps:** update dependency mypy to v1.8.0 ([29b1db9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/29b1db9c22aea4a376d7f75a39f0b719b2b29936))
* **deps:** update dependency packaging to v23.2 ([585ac2d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/585ac2d739f8d1aeed0a9fc368dbeceb1c6874d3))
* **deps:** update dependency packaging to v23.2 ([840cecf](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/840cecfafdb126c50280c6d5c31feb2527befe71))
* **deps:** update dependency pygit2 to v1.12.2 ([24233b4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/24233b4000c08ee841351d662b3f408be970e31e))
* **deps:** update dependency pygit2 to v1.12.2 ([7cef2ed](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7cef2ed9d9fbf456a869430df260409321b41e55))
* **deps:** update dependency pygit2 to v1.13.0 ([efbe108](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/efbe108efea65d9e9727f43b86fb2fbe58805f89))
* **deps:** update dependency pygit2 to v1.13.0 ([a4309e5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a4309e50c481128c6b96c1b7c5a66fd44dd1da85))
* **deps:** update dependency pygit2 to v1.13.1 ([8eefd2d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8eefd2d27482a0594a1bb633d396fa1b1a0339cb))
* **deps:** update dependency pygit2 to v1.13.1 ([773d4f5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/773d4f52efa3b8e7b005e8f9a14c239f2c5fd020))
* **deps:** update dependency pygit2 to v1.13.2 ([ea4fc0a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ea4fc0a6b036135abf47f1d698ee97c32290cc1b))
* **deps:** update dependency pygit2 to v1.13.2 ([c06385c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c06385cd72eb6d51460a4a16019db51e18a2ea2d))
* **deps:** update dependency pygit2 to v1.13.3 ([0148e6d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0148e6d601cf2b9e6146da82cc98405fc3e19fbd))
* **deps:** update dependency pygit2 to v1.13.3 ([2082d7f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2082d7f2c2e7fa4ed030da99447c15ccf1cdd99f))
* **deps:** update dependency pygit2 to v1.14.0 ([560dcc9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/560dcc988f3978062bfb801df3939dad02ff250d))
* **deps:** update dependency pygit2 to v1.14.0 ([2867a3c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2867a3c5ab76063f28d37de776caaff31aba4e71))
* **deps:** update dependency pygit2 to v1.14.1 ([2f4c5ca](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2f4c5cac14fc8a7c95c5c9626fe81b44fa7e2306))
* **deps:** update dependency pygit2 to v1.14.1 ([195beda](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/195beda01fce1335694f9c0dd1851c6de1c0428f))
* **deps:** update dependency pygithub to v1.59.0 ([2eba231](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2eba23188250f592669eb79fd92681fcaa34eeac))
* **deps:** update dependency pygithub to v1.59.0 ([0d5411e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0d5411ee86b52c090b979198fe0f00eb5f524659))
* **deps:** update dependency pygithub to v1.59.1 ([9c9851f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9c9851f546e75cdb443d45b531dcdcbd822102ec))
* **deps:** update dependency pygithub to v1.59.1 ([967b578](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/967b5782772e1ec892d3b89a037a447d42675d7b))
* **deps:** update dependency pygithub to v2 ([73bbadd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/73bbadd86828b02666a6e9249ed921ad0eae2f79))
* **deps:** update dependency pygithub to v2 ([5d81f4b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5d81f4bd66e7dd7daf9432393487258d4940f5ca))
* **deps:** update dependency pygithub to v2.2.0 ([9261a89](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9261a89b452840a0e36ab0b9312c5a491376ed6b))
* **deps:** update dependency pygithub to v2.2.0 ([a412e43](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a412e43d9edb3ac5067700f29ea3dcee176b0c86))
* **deps:** update dependency pylint to v2.17.5 ([afc347d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/afc347d01a4fb7b5966677dd2a7fae0bcb45e98f))
* **deps:** update dependency pylint to v2.17.5 ([816b9c2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/816b9c272757152398a00d99151973081b568db2))
* **deps:** update dependency pylint to v2.17.6 ([b3fee6c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b3fee6ce965ec797d62f39791de1533e00b1c1b3))
* **deps:** update dependency pylint to v2.17.6 ([1b2d040](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1b2d040fbf0cee1d48399ae98aea5087e631b288))
* **deps:** update dependency pylint to v2.17.7 ([b158671](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b158671fa235cb2fb65adc2a2cefbb6bd03bbdb0))
* **deps:** update dependency pylint to v2.17.7 ([66a2682](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/66a268257a4de0e6aaeeb921d2b2b98819d8f30d))
* **deps:** update dependency pylint to v3 ([0dcebb1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0dcebb1fa21fd94a8d6a42de9eba2d04c9193687))
* **deps:** update dependency pylint to v3 ([d978716](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d978716d984182f6f3d6259b7fda5a383afe27c1))
* **deps:** update dependency pylint to v3.0.1 ([ce2375e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ce2375e8138a0d552bc0185089b665d7addbca18))
* **deps:** update dependency pylint to v3.0.1 ([8cf93fa](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8cf93fa72dab866df2e11d177410f4724ec813f0))
* **deps:** update dependency pylint to v3.0.2 ([6e4dcba](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6e4dcba8fcbb8ace738751ca65cce203f6c374c4))
* **deps:** update dependency pylint to v3.0.2 ([674aaaa](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/674aaaa68508b02defd22ab98921961eb8c007ac))
* **deps:** update dependency pylint to v3.0.3 ([8c2f3fa](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8c2f3fa822e7f36fd0d66d3268d87d8755ba96d9))
* **deps:** update dependency pylint to v3.0.3 ([e62410c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e62410cead81f3304495c6067b27bf5b1ab1706f))
* **deps:** update dependency pylint to v3.1.0 ([a014787](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a014787eec6424c702f0c6ca1aa969ee77c0de83))
* **deps:** update dependency pylint to v3.1.0 ([ec87e10](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ec87e10b20cacce6529a51d6a00c1167668441e3))
* **deps:** update dependency pytest to v7.4.0 ([1d477c5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1d477c59f09dfe8e69f7a4e9e095cf0ee9ec96dd))
* **deps:** update dependency pytest to v7.4.0 ([a49cf9a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a49cf9a80e837167e25c4336002ab271ab157f21))
* **deps:** update dependency pytest to v7.4.1 ([9d9cf2f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9d9cf2f378e1ed1d18c867a3d6a2956d74f2cafc))
* **deps:** update dependency pytest to v7.4.1 ([bc4e82e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bc4e82eb1b89d0d7b0a101793915baf664123ee4))
* **deps:** update dependency pytest to v7.4.2 ([21198d7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/21198d78c99a293e92db8399d472ffd3d447067c))
* **deps:** update dependency pytest to v7.4.2 ([ae37bd7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ae37bd7b552d0f5599a18744d2a70cbdb28a6f37))
* **deps:** update dependency pytest to v7.4.3 ([aefb4ed](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/aefb4ed0277be1052eebff0c98a46a2f32512d57))
* **deps:** update dependency pytest to v7.4.3 ([43ef1a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/43ef1a3a930c2c5e9d4402a3efa76a4f26dd7e39))
* **deps:** update dependency pytest to v7.4.4 ([9502ae4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9502ae486b53dcf49d552806da89f729dcad9656))
* **deps:** update dependency pytest to v7.4.4 ([3503f4b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3503f4b66eb0b506cf31c766ff401bf1ee006395))
* **deps:** update dependency pytest to v8 ([a4fbe29](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a4fbe290e50caa639271ccc760137c59d6020ec8))
* **deps:** update dependency pytest to v8 ([a1d46f2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a1d46f24352a87998019884b6e4f7b9619616eba))
* **deps:** update dependency pytest to v8.0.2 ([6d37167](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6d37167baf6c031ebef0699b7991e9bec2aa5b7f))
* **deps:** update dependency pytest to v8.0.2 ([0328bac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0328bacc1c3942e22a85723793542dc7e33e2bd1))
* **deps:** update dependency python-gitlab to v4 ([51400e9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/51400e9198731035a9ed3c48e326c5d43c7c5c0c))
* **deps:** update dependency python-gitlab to v4 ([27bd3b9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/27bd3b98b42b3f558a6939200af596358573db78))
* **deps:** update dependency python-gitlab to v4.2.0 ([c4174bb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c4174bb21e9ad08287fed7ff3349ae51033a0c22))
* **deps:** update dependency python-gitlab to v4.2.0 ([818a378](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/818a3785269157f44a6bd808236f158d89ed76ca))
* **deps:** update dependency python-gitlab to v4.3.0 ([3c25ba4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3c25ba47508ca9412860ea59aab6adfc19b1ec18))
* **deps:** update dependency python-gitlab to v4.3.0 ([8208fa3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8208fa3666c061d11c07e93cf6914ccbaff5b8ff))
* **deps:** update dependency python-gitlab to v4.4.0 ([453256a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/453256a2029eef94bd729429ea1a46c10267c35e))
* **deps:** update dependency python-gitlab to v4.4.0 ([2bb6fae](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2bb6fae399694a859de7b0759654af55ea6885d3))
* **deps:** update dependency pyyaml to v6.0.1 ([b41ce04](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b41ce04fd3a591f84e92ecb2f0145d28bdd76888))
* **deps:** update dependency pyyaml to v6.0.1 ([aea0015](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/aea0015ba2408543e429742dde3f707a94e786fa))
* **deps:** update dependency types-html5lib to v1.1.11.15 ([5cf0bb0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5cf0bb004468efd8d4aa27369e817dc112e3c108))
* **deps:** update dependency types-html5lib to v1.1.11.15 ([e2ad704](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e2ad704ab4587fa9567824e40ede45eea0090804))
* **deps:** update dependency types-html5lib to v1.1.11.20240106 ([99a7fa6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/99a7fa6886125bf4c5369634788fbd285b8fbc38))
* **deps:** update dependency types-html5lib to v1.1.11.20240106 ([65d2e18](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/65d2e18780e01fcf28e60c031f3db89cb4d945f7))
* **deps:** update dependency types-html5lib to v1.1.11.20240217 ([64e9eef](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/64e9eef1e185b7c2935756c318424999ef15a082))
* **deps:** update dependency types-html5lib to v1.1.11.20240217 ([4ce70bb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4ce70bbd97554bfdeb6e3ba4b5e0ff74db04932f))
* **deps:** update dependency types-pyyaml to v6.0.12.11 ([68df5b0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/68df5b0e05e61253edf8baa852e22d1786d6ea04))
* **deps:** update dependency types-pyyaml to v6.0.12.11 ([2b725e4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2b725e411cecf4669b9f6da85616a33468500b3e))
* **deps:** update dependency types-pyyaml to v6.0.12.12 ([bdc3e28](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bdc3e2852d4c2a541fba14273f24c7f3c5849fe7))
* **deps:** update dependency types-pyyaml to v6.0.12.12 ([b5c65a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b5c65a3ec0054cde94fde43d41638d0588419f90))
* **deps:** update dependency types-requests to v2.31.0.10 ([653ca2d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/653ca2d6cbb4fbcf27ef93c9b9e4a1cf49630789))
* **deps:** update dependency types-requests to v2.31.0.10 ([92e08fd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/92e08fd490373652c082fba791d45b7caded37b8))
* **deps:** update dependency types-requests to v2.31.0.2 ([7afb54f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7afb54f79cf75ca8fe7d902f902fb794d918b027))
* **deps:** update dependency types-requests to v2.31.0.2 ([960d52a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/960d52a1b0b9bb08e721d0975c5b15c31bf21b87))
* **deps:** update dependency types-requests to v2.31.0.20240106 ([c56df5f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c56df5f120e09c081f3262caae55088a41be6a4c))
* **deps:** update dependency types-requests to v2.31.0.20240106 ([6e0fcef](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6e0fcefc05271f645540b21dcb4afccd86094bbb))
* **deps:** update dependency types-requests to v2.31.0.20240218 ([81585c9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/81585c91c1d67dd19a60f5a583371d348851de19))
* **deps:** update dependency types-requests to v2.31.0.20240218 ([395ffa7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/395ffa7f50266ffdc2065cf238434dad97af28e0))
* **deps:** update dependency types-requests to v2.31.0.5 ([f571521](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f571521a9fef5fb3af5cf2962c41e5882feaf3b4))
* **deps:** update dependency types-requests to v2.31.0.5 ([db47c4f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/db47c4fc513de452c4b930e1cab34b11de0e25e3))
* **deps:** update dependency types-requests to v2.31.0.7 ([c5307b7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c5307b7809bbca4811354c89efca777d37c5f7a2))
* **deps:** update dependency types-requests to v2.31.0.7 ([79c0b72](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/79c0b72c2f812390a9f66038d71722ab007fb81f))
* **deps:** update dependency types-requests to v2.31.0.9 ([8d65176](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8d65176c37c6e24e693ce16d057c724b3d8ceae0))
* **deps:** update dependency types-requests to v2.31.0.9 ([02d0476](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/02d047666d7b3c081f48337c98cc1ec73e609418))
* **deps:** update dependency urllib3 to v1.26.17 ([4c3dbff](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4c3dbffd557e78b50861786d38f38817e9c0383e))
* **deps:** update dependency urllib3 to v1.26.17 ([471395b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/471395b6185aba5dc79d6a7d5303860f25b7a346))
* **deps:** update dependency urllib3 to v1.26.18 ([95b455f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/95b455f36e4198aa6cdfc204a0d433a1fdfa7f95))
* **deps:** update dependency urllib3 to v1.26.18 ([0370cbc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0370cbc4240a540591729bf3b92acd53e8622205))
* **deps:** update dependency vcrpy to v4.4.0 ([7aa53b8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7aa53b89b831670c5269b7625ffeb5b4ba78b460))
* **deps:** update dependency vcrpy to v4.4.0 ([dc94c10](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dc94c10744d078d9ac890988843cfe28cda0a2d2))
* **deps:** update dependency vcrpy to v5 ([7347977](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7347977d7408971039f1f523f3dcd4b41b05a1fe))
* **deps:** update dependency vcrpy to v5 ([6af373c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6af373c5527aaa550a43cd94d594e152b63242d6))
* **deps:** update dependency vcrpy to v5.1.0 ([c71e611](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c71e61105436fca1d369d80973490618623d687a))
* **deps:** update dependency vcrpy to v5.1.0 ([cdaab45](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cdaab45264d18d8bda6c0a357b12a408a7a21f40))
* **deps:** update dependency vcrpy to v6 ([42542b2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/42542b2c45d785b87ce266174813f4f932a76dcb))
* **deps:** update dependency vcrpy to v6 ([e1cfc8a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e1cfc8af249e2525f1501cb56c6db141a1682e4f))
* **deps:** update dependency wemake-python-styleguide to v0.18.0 ([4190bb4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4190bb4b033be9e43d69b745d50adb9d68873399))
* **deps:** update dependency wemake-python-styleguide to v0.18.0 ([6b862f3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6b862f3d67a07049a45c04a2ce447247a995725b))
* **deps:** update docker docker tag to v25 ([d763772](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d763772d4ef056044d0cc9d00079d1140cc1cbd4))
* **deps:** update docker docker tag to v25 ([1b0857e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1b0857ed8bbcec6d6ce8c4bcd4aa7340f3b9ff7a))
* **deps:** update node.js to v20.3.1 ([d9f14c7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d9f14c7200c0ab89562a4981c7d943e7c82f32b0))
* **deps:** update node.js to v20.3.1 ([749ff84](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/749ff84bf0d83caded1d2497788f9f1737969c22))
* **deps:** update node.js to v20.4.0 ([adc83ad](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/adc83ad616d16119cd95298c960944e470ff4a28))
* **deps:** update node.js to v20.4.0 ([4efa31b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4efa31b1afe3060598fdce79e7cf4dab8543aaf6))
* **deps:** update node.js to v20.5.0 ([0f7ae07](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0f7ae076f48b3b8ddd9e94715fa112053eceb893))
* **deps:** update node.js to v20.5.0 ([f5e277f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f5e277f7aa3026dfe43586a33c9b05a11c126fe2))
* **deps:** update node.js to v20.5.1 ([44b04e9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/44b04e959722a6599edb79ce1df5f10d3be8a8cf))
* **deps:** update node.js to v20.5.1 ([0376aa6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0376aa69d225e04602f0fff60b18a1329bc4dbd2))
* **deps:** update node.js to v20.6.1 ([9b46183](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9b461833a318bf6ab1fb07e8dc3f1d891191be3b))
* **deps:** update node.js to v20.6.1 ([9cfe9da](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9cfe9daaee4a5241f8ccd3463ea5363282c04e49))
* **deps:** update node.js to v20.7.0 ([0d53699](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0d5369958ef58041c0e5b2f33501e0cdbe52bc40))
* **deps:** update node.js to v20.7.0 ([1cfd46a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1cfd46a153c48e27162d61fbde094cb57c49ead2))
* **deps:** update node.js to v20.8.0 ([f6ff6bf](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f6ff6bfe054c860554c79069b47458e1bf11cbfb))
* **deps:** update node.js to v20.8.0 ([642c181](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/642c18172a50cc7d47d81f868a989f4df30d4e65))
* **deps:** update node.js to v20.8.1 ([c7c175f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c7c175f2b6b8d4bff639974d15c50b6aa4e9cf65))
* **deps:** update node.js to v20.8.1 ([1950419](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/195041974f4043961c4e6cd65aa3168f349fd525))
* **deps:** update node.js to v21 ([301ac90](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/301ac90776e03d4ea7c509913a47b438416b32e3))
* **deps:** update node.js to v21 ([f5e9591](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f5e9591dcaf2a55d35227919c55ff5ceb71b4bf4))
* **deps:** update node.js to v21.1.0 ([777f532](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/777f5325f89562414571b9b1dace9eaaaaf45332))
* **deps:** update node.js to v21.1.0 ([3d70016](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3d70016862fb9978800aca2f173f7747e9b18f8d))
* **deps:** update node.js to v21.2.0 ([a10b964](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a10b9646d4c27282cc0f389e703200cce213b7e1))
* **deps:** update node.js to v21.2.0 ([efabeb1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/efabeb124dab98e93ef0b908b41414bd737ca00a))
* **deps:** update node.js to v21.3.0 ([53a777d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/53a777d7098c7897b368c37338cbb7a8e74b6d1f))
* **deps:** update node.js to v21.3.0 ([8e14411](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8e14411901f02f755de0e33290a589c0ff3c92a8))
* **deps:** update node.js to v21.4.0 ([8eba79c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8eba79c555a9231e601e46802c95431019bb6eba))
* **deps:** update node.js to v21.4.0 ([1dd3dfd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1dd3dfd7363a2cb126f166f9d58ad661c1c07c08))
* **deps:** update node.js to v21.5.0 ([4eb8d93](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4eb8d93a057bec1216c86d89aa95207668bb2435))
* **deps:** update node.js to v21.5.0 ([0fabfcb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0fabfcb16f79e17117ec12a882f2ef1245d663a9))
* **deps:** update node.js to v21.6.0 ([52de4a6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/52de4a68ad0d4d04fb99eb78ddcc4a22df5e2c20))
* **deps:** update node.js to v21.6.0 ([a87b789](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a87b789cce5a3809a071e104b0ca39bb841a730f))
* **deps:** update node.js to v21.6.1 ([1481ec6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1481ec6fc59dd80cd618d5aa9cd167441e95f2c5))
* **deps:** update node.js to v21.6.1 ([99a2650](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/99a2650357f181fb26957266dd7923d3a31a6821))
* **deps:** update node.js to v21.6.2 ([7818a55](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7818a551478ab4dfbaa241ee3bd933fb1689da88))
* **deps:** update node.js to v21.6.2 ([d5df02b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d5df02bf7446ca44fcb22928a79625a40882890e))
* **deps:** use bookworm as base image ([442e52b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/442e52ba17175605b7ef53835dbfa8ce4af88346))
* drop support for Debian buster ([5a01a55](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5a01a55e854975b2f11c36883360bbccaa68f468))
* drop support for Debian buster ([f766ff1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f766ff178bc2e526e66fa1e202498d06e35a3e2e))
* ignore mypy type-var error ([f404ab6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f404ab61c1d7e14d1316ce6dacd7546d2a691c86))
* **pip:** use inline typing ([3197f33](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3197f338fb0658d2a2b1cc24ef3b38ed655f557f))

## [1.12.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.11.0...v1.12.0) (2023-01-17)


### :repeat: Chore

* **deps:** do not pin flake8-rst-docstrings ([74a52bc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/74a52bc43127f1b319a58cb15aaeb93f04457220))
* **deps:** update dependency flake8-bugbear to v22.10.27 ([cb13eb4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cb13eb4a7d412cff67ee1f92ce9de2c6a6cdeff2))
* **deps:** update dependency flake8-bugbear to v22.10.27 ([394e81a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/394e81a8c7c7f7a52799618ff85bdee0628c8a8e))
* **deps:** update dependency flake8-bugbear to v22.12.6 ([ea67bb5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ea67bb5b1d98af3d2880606bfca9907b66cf6b36))
* **deps:** update dependency flake8-bugbear to v22.12.6 ([46766ba](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/46766ba07c74992c49fcefdc888ad26a6fbc2057))
* **deps:** update dependency flake8-builtins to v2.0.1 ([0449657](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0449657c98be6d0179f1c17ed3e4920a5eb6ebaf))
* **deps:** update dependency flake8-builtins to v2.0.1 ([846d2f1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/846d2f1deaf39bfb4de2466961e53ada3632796f))
* **deps:** update dependency flake8-builtins to v2.1.0 ([b521bd7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b521bd7c5f4c8731681cae12f707a79286a2df4b))
* **deps:** update dependency flake8-builtins to v2.1.0 ([70f670b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/70f670be5f56140d736e98f3b0ad758ed089d1dd))
* **deps:** update dependency flake8-comprehensions to v3.10.1 ([730c608](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/730c608ea3d5b7ee677402ec55178b6a42d00005))
* **deps:** update dependency flake8-comprehensions to v3.10.1 ([9bb1d4e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9bb1d4ef28c235ee1984a6fe8e49a7fc5071bfc4))
* **deps:** update dependency flake8-logging-format to v0.9.0 ([a57f4fa](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a57f4fa250cdca2439501777cbce45a52c341d02))
* **deps:** update dependency flake8-logging-format to v0.9.0 ([9d9500b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9d9500b0ae7d28bf691a0f17ce5b4971fec4b171))
* **deps:** update dependency flake8-quotes to v3.3.2 ([258abac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/258abac7c035193e1d0b5cf6dac93b3c1f8220a8))
* **deps:** update dependency flake8-quotes to v3.3.2 ([fef7b5e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fef7b5eeea1746c32b87b74f07a008eb8e1f21aa))
* **deps:** update dependency flake8-rst-docstrings to v0.3.0 ([6284d8a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6284d8a1a00864d5c1c22310b012c1f2ad4bf732))
* **deps:** update dependency flake8-rst-docstrings to v0.3.0 ([53b7921](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/53b7921f64a7b2ce4a4cf95c36a5c96883ebcc09))
* **deps:** update dependency isort to v5.11.1 ([86ab591](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/86ab591df29d4a78547df06c5db6aae75fbe821c))
* **deps:** update dependency isort to v5.11.1 ([4fc6d1a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4fc6d1a629b3657811a48ddccf671112f17bfcd9))
* **deps:** update dependency isort to v5.11.3 ([f883955](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f883955ed4c445cda463a7b33d45f0c34107bf7d))
* **deps:** update dependency isort to v5.11.3 ([c108dfd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c108dfd33d64cd1f71a349a9492608e00157fb44))
* **deps:** update dependency isort to v5.11.4 ([4f22313](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4f223135626786e43aa8de594d99397f35629efb))
* **deps:** update dependency isort to v5.11.4 ([8b22d16](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8b22d16f9db2e724dbceaf141d06fa467efda20f))
* **deps:** update dependency jsonschema to v4.17.0 ([20d7e24](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/20d7e24158fcb4d36913c3c1f35eef3d9f653aef))
* **deps:** update dependency jsonschema to v4.17.0 ([a035368](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a0353683b06d668f31c056c808706f26ecc32868))
* **deps:** update dependency jsonschema to v4.17.1 ([f57320d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f57320dad55c26b9acc64f0604c251b957e01bce))
* **deps:** update dependency jsonschema to v4.17.1 ([250370d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/250370df5b9c9824ea9dff3735d52a2d0ccb3774))
* **deps:** update dependency jsonschema to v4.17.3 ([9a4f3db](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9a4f3dbc1e0f0158aa9c1f2a578c2cab0bb281ec))
* **deps:** update dependency jsonschema to v4.17.3 ([714e2a9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/714e2a9fc846a4756880f16b3632ec50fda6c89b))
* **deps:** update dependency mypy to v0.990 ([a8d0e59](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a8d0e59f924ad5e9a9b429e159ce1a5b6ce6ecc4))
* **deps:** update dependency mypy to v0.990 ([2a43654](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2a436545ce5824616e9bf44f287fbd5314b5b0ce))
* **deps:** update dependency mypy to v0.991 ([35c3351](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/35c3351eec9ec42468cd6db28c0b1e1342cce163))
* **deps:** update dependency mypy to v0.991 ([5996388](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/59963889c8467e1a63612bd8284a2a12ec9ffc3f))
* **deps:** update dependency packaging to v22 ([f4c3ef0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f4c3ef0b87e2c9f405cd0c5f3527be610dbaaf3d))
* **deps:** update dependency packaging to v22 ([b0a4a79](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b0a4a798cfb7630c2ef1dd6337a30473a5edea6e))
* **deps:** update dependency packaging to v23 ([ba1c435](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ba1c4359b6a31d2ca65076edbfe2e1daa96329ec))
* **deps:** update dependency packaging to v23 ([611d52a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/611d52a665e69215adbe61ada5e53e062383111a))
* **deps:** update dependency pygithub to v1.57 ([8f1e6ee](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8f1e6ee7a4a4550ad74262c183dadc86e39bb227))
* **deps:** update dependency pygithub to v1.57 ([923560f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/923560fb0caf59e4dea9ac6c44769d02e08ddae7))
* **deps:** update dependency pylint to v2.15.10 ([ad6607f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ad6607f8ec884a09a02ed56ff3a6dc1a96a4bd3d))
* **deps:** update dependency pylint to v2.15.10 ([be7a14c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/be7a14ccb5cf68fe94944713aac3f48793d5e17a))
* **deps:** update dependency pylint to v2.15.6 ([a65325b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a65325b435dd8400ca141dc4ac233677ac2fe882))
* **deps:** update dependency pylint to v2.15.6 ([2642f50](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2642f50d2b5c945207501dfedf38218e7bf8a17a))
* **deps:** update dependency pylint to v2.15.8 ([4b6016b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4b6016b9f98cbff75b983dd48cbd410d933c72a9))
* **deps:** update dependency pylint to v2.15.8 ([22ab38f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/22ab38ff6437187503e707e7ca29185320b41735))
* **deps:** update dependency pylint to v2.15.9 ([d5ba390](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d5ba390c71da9f087e7eef4f4a5a8b326d26c9ee))
* **deps:** update dependency pylint to v2.15.9 ([a9bfeb1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a9bfeb1afb478bbd86e94c86d40a0568a9c56290))
* **deps:** update dependency python-gitlab to v3.11.0 ([da19dab](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/da19dabce3fe88e0f6ca97aa545f3f0b136abb2f))
* **deps:** update dependency python-gitlab to v3.11.0 ([bdd12ca](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bdd12cabd15a1241091e5395e2a0a633bf8112f7))
* **deps:** update dependency python-gitlab to v3.12.0 ([0d4b25e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0d4b25ee97c14d54b0bd3db8422b9cf0aee5035c))
* **deps:** update dependency python-gitlab to v3.12.0 ([88a683b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/88a683bd2dfbe75ef23df3c2ae9e8d7dde21c01e))
* **deps:** update dependency types-html5lib to v1.1.11.1 ([dc76943](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dc769431e7e8ab9cbaacdc868ce82028ea8680a1))
* **deps:** update dependency types-html5lib to v1.1.11.1 ([7ed6951](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7ed695142e5bb64ed82823baaa461f6c723b3df8))
* **deps:** update dependency types-html5lib to v1.1.11.10 ([ff59b3e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ff59b3ec04e07301a92d7bb96855ef120de003ee))
* **deps:** update dependency types-html5lib to v1.1.11.10 ([e2513ba](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e2513baedea1b7243606575e4678e909d86ae3a7))
* **deps:** update dependency types-pyyaml to v6.0.12.1 ([ac287af](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ac287afe593d0c35139487a679d10b48c932746c))
* **deps:** update dependency types-pyyaml to v6.0.12.1 ([87b8016](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/87b8016664f26abb601f16ad6d647ef7cba58215))
* **deps:** update dependency types-pyyaml to v6.0.12.2 ([b44c4ed](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b44c4edd3cebe64eb4069960318569fcd83bb5d4))
* **deps:** update dependency types-pyyaml to v6.0.12.2 ([4ac2d14](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4ac2d14606461485aa1e4435cf369babf3542cf4))
* **deps:** update dependency types-requests to v2.28.11.4 ([844844f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/844844f4769366cb884a4e9075048d29ebf571c4))
* **deps:** update dependency types-requests to v2.28.11.4 ([4c981fd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4c981fdcd5985854ef1f8b0ac72b6728475b083b))
* **deps:** update dependency types-requests to v2.28.11.5 ([dbff192](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dbff192549e133763d70218adbdeafaff6a84803))
* **deps:** update dependency types-requests to v2.28.11.5 ([9a98db0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9a98db094e8a6a5de52607b89119addc36002030))
* **deps:** update dependency types-requests to v2.28.11.7 ([2974984](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/297498491b979378e3636fa340e3a028741d554c))
* **deps:** update dependency types-requests to v2.28.11.7 ([283c4a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/283c4a3be662401cdaaf5af7db4b2e0067c13a75))
* **deps:** update node.js to v19.1.0 ([5726125](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5726125be3ffeb34dc6281f91d60b30d04c354f7))
* **deps:** update node.js to v19.1.0 ([0c8712b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0c8712bcb20ca4e312f6779a746918fcdf5059fe))
* **deps:** update node.js to v19.2.0 ([c46c927](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c46c92705e75956dfcab06eb028d07a432b92bf5))
* **deps:** update node.js to v19.2.0 ([613e82d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/613e82d6c2f84380a6a264cb0580519e6b1fa145))
* **deps:** update node.js to v19.3.0 ([669a947](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/669a947385d685068ac74020cad14d7c65e2c0c3))
* **deps:** update node.js to v19.3.0 ([5e15643](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5e156432565086779fd360085f4ec398a0f3c664))
* **deps:** update node.js to v19.4.0 ([7c6ee3d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7c6ee3d5c1720a03306bb46b83deba2229518074))
* **deps:** update node.js to v19.4.0 ([b3fe0a7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b3fe0a7736fd29558361e6b8619459f4df1617e5))
* **gitlab:** do not pass None password ([a32fc4b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a32fc4bb10a4a5f4e84f2eba3fafceeae0e9b4d9))


### :sparkles: Features

* **github:** token thru GITHUB_TOKEN env ([1ef9b54](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1ef9b54415b4765edad4c6fcf1f698b1d959b7d3))
* **github:** token thru GITHUB_TOKEN env ([3257767](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/325776743ef5d9cf562a3a36aa9e0192db00c640))


### :memo: Documentation

* **release:** use scoped npm packages ([e8bfaa0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e8bfaa0f79da1ccc7e650ff4879fb9039abbc5a9))

## [1.11.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.10.0...v1.11.0) (2022-10-26)


### :bug: Fixes

* **debian:** fix repeated-path-segment doc ([9b42c94](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9b42c947385bb847febe216e2d76c3c11de96caa))


### :sparkles: Features

* **image_mirrors:** add enabled parameter ([c7c303b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c7c303b47dd2dfee6db9b8f6154092836d06982c))
* **pacakge_mirrors,image_mirrors:** retry backoff ([bbe4427](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bbe4427da1c47e34c9ae08e568576ec4a69f5d97))
* **pacakge_mirrors,image_mirrors:** retry backoff ([01f81b7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/01f81b70d9003961963e306c3014fe46ffbba467))
* **package_mirrors,image_mirrors:** add enabled parameter ([6b40466](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6b40466d3a77286bcc8ed5d6f772222b2f6b7b81))
* **package_mirrors:** add enabled parameter ([c9b9714](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c9b97147dc36a18b2e95d0c9a840c1e384c34804))


### :repeat: Chore

* **ci:** make most jobs manual ([9e2a688](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9e2a688efc5ffaafd620c08e34640e940de7c87b))
* **ci:** make most jobs manual ([b6c6288](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b6c62887239a19d7d8e784545797f8b19d704b10))
* **copyright:** update copyright to 2022 ([d34b1b4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d34b1b4fa0e7ebc0717cd31d889c8f4d5edfebdb))
* **copyright:** update copyright to 2022 ([83ce01f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/83ce01f514b5c74952fe42934b23e3450a8d16c6))
* **copyright:** update copyright to 2022 ([932f029](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/932f029fe22b519bfcceb20dfe090fdfd0be7c33))
* **copyright:** update copyright to 2022 and other packaging fixes ([98bc8d7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/98bc8d72b106e4c695cb199894e0326f29888ceb))
* **debian:** policy 4.6.1 ([72bcf73](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/72bcf73bb00382550241124f3d6a70fec65bf535))
* **deps:** do not pin wemake-python-styleguide dependencies ([1da7dac](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1da7dacfb86fd16880018f2b18c4e33ecf5a89b4))
* **deps:** do not pin wemake-python-styleguide dependencies ([3540d13](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3540d13fa3d46008936de988e2a0b46689893706))
* **deps:** update dependency flake8-broken-line to v0.5.0 ([b59b113](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b59b1133d9023e037c530f85a427ef6a362a347c))
* **deps:** update dependency flake8-bugbear to v22.10.25 ([695113e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/695113e06cccf48767662bf43fe15f5e04645b2c))
* **deps:** update dependency flake8-bugbear to v22.10.25 ([71d6799](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/71d679929c87053ac82659bb8a5318806b293dbd))
* **deps:** update dependency flake8-builtins to v2 ([7e45dde](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7e45ddecd16a33962369cfb33db12c8c3fe8500f))
* **deps:** update dependency flake8-builtins to v2 ([7200934](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7200934d148ee7fb7f6ce236ad60122421d70213))
* **deps:** update dependency flake8-deprecated to v2 ([a5602ee](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a5602ee7d5047a256d29df744d51b1bdc7a0f08b))
* **deps:** update dependency flake8-deprecated to v2 ([638c00a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/638c00ac14c6a4ac62159aac7002c361b25a28fd))
* **deps:** update dependency flake8-logging-format to v0.8.1 ([c17f8ce](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c17f8ce53d3b25074f77768a507de64b59dc2c9a))
* **deps:** update dependency flake8-logging-format to v0.8.1 ([1419631](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/14196311b675494c53c97c2da54fefcfbcd9876c))
* **deps:** update dependency mypy to v0.981 ([0bd191b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0bd191bdd81900a5ad1e218f8a745e3f5ee226b4))
* **deps:** update dependency mypy to v0.981 ([603aca0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/603aca02152253fd164e213d6ef5d299e542befb))
* **deps:** update dependency mypy to v0.982 ([80d4898](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/80d48983ddd0be593d89c11edd1e59bf50a174ba))
* **deps:** update dependency mypy to v0.982 ([14ec802](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/14ec80269945b6bd46b8fca08ef89a134ddaa85d))
* **deps:** update dependency pygit2 to 1.10.1 ([16dcc31](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/16dcc3177e788f15098997a1cbe0e37ea8e385d6))
* **deps:** update dependency pygit2 to 1.10.1 ([f17eb10](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f17eb10eaf4c75b2dec3cbdfee0104ad190cf2f5))
* **deps:** update dependency pygithub to v1.56 ([6f80f9d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6f80f9d0f130e254e8397eeb9bf97608efe9bd2d))
* **deps:** update dependency pygithub to v1.56 ([7b25bff](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7b25bff6040811cc3de2d0b6a1ab840e3febc5b8))
* **deps:** update dependency pylint to v2.15.4 ([add9d52](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/add9d5261a125057031b3e2d1b3a28c350882e7f))
* **deps:** update dependency pylint to v2.15.4 ([9b8a2c9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9b8a2c96a7bf9dadd463ec367be82ed1720ee4ac))
* **deps:** update dependency pylint to v2.15.5 ([c4a9e16](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c4a9e1699d8970cb7ddaeb74533502ccdeb98bb4))
* **deps:** update dependency pylint to v2.15.5 ([f72cfb1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f72cfb1c9d61e7a18ccae9f37417c5ccb2d85038))
* **deps:** update dependency pytest to v7.2.0 ([eb1b389](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eb1b38955c0972f6cbd5cadf1de07c15c4d14f15))
* **deps:** update dependency pytest to v7.2.0 ([0bf7e87](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0bf7e8730cd324c3db1bcee0df89abe5cb33c0c7))
* **deps:** update dependency python-gitlab to v3.10.0 ([2e7064a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2e7064a400940d079d400ce889f4f21ca38a15cb))
* **deps:** update dependency python-gitlab to v3.10.0 ([5dec8ea](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5dec8ea4a008d491b0c7f7c449736eb2c627cde5))
* **deps:** update dependency types-pyyaml to v6.0.12 ([eb33ccb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eb33ccbb2c67e4fe8eff2dea371a474466c669d0))
* **deps:** update dependency types-pyyaml to v6.0.12 ([9b279a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9b279a3c88889c2364d8b07e17ea2634680120db))
* **deps:** update dependency types-requests to v2.28.11 ([b8d47ae](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b8d47ae225cfc356d8954e94337cd18c00676675))
* **deps:** update dependency types-requests to v2.28.11 ([9fef0e9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9fef0e99ee8fd80a56fe9d51e55df321bdb11a7c))
* **deps:** update dependency types-requests to v2.28.11.1 ([6ad2867](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6ad2867a0090be092c761ca6cfb420ff12883a19))
* **deps:** update dependency types-requests to v2.28.11.1 ([fe57f7c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fe57f7cb7c5cedf315852b38c79fab3cc88911bd))
* **deps:** update dependency types-requests to v2.28.11.2 ([a3aad5b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a3aad5b08eff98231193a4d7ceefc8e9f0e5d6d7))
* **deps:** update dependency types-requests to v2.28.11.2 ([61351a0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/61351a09676122aca654b5797838936191e7c4c1))
* **deps:** update dependency wemake-python-styleguide to v0.17.0 ([2db3993](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2db39935f3a846412192023e9bc7c7aa9d4161de))
* **deps:** update dependency wemake-python-styleguide to v0.17.0 ([351440d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/351440d29f70db2e396147365c0ff23e0e2a9a4d))
* **deps:** update node.js to v18.10.0 ([33819e0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/33819e07f40ca26b7f30489e2a2df448a2aceadf))
* **deps:** update node.js to v18.10.0 ([9704327](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9704327d97a51a51bfc2f3eeab5d8eab47fa5699))
* **deps:** update node.js to v18.11.0 ([7b31419](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7b31419c634f5ba684f6cd47975b1809e02511b2))
* **deps:** update node.js to v18.11.0 ([27c5ff7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/27c5ff771cafb8559667afa077489d60576c621a))
* **deps:** update node.js to v19 ([ba4ee4b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ba4ee4bc2250b26f5f8657ead62d68b1d61b75ae))
* **deps:** update node.js to v19 ([2add4f3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2add4f38da79bcce157095ae6423468f4ed175ee))
* **renovate:** ensure renovate catch pygit2 updates ([9024df2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9024df2568039ac15952aa848356afd79fa9d420))
* **typing:** removing two obsole type: ignore ([b738fad](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b738fad723ce6f04ee61617830bc99973e1437e1))


### :memo: Documentation

* improve release docs ([7682e47](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7682e47410a80fb319e26eeddb7e1badd999a410))
* two spelling fixes ([449d302](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/449d3025d1e7c7b9fabbf2f0e9851d99503dc9a2))
* update release.md ([643427e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/643427e32d6509e3f0e8f86bc7a577622b02c739))

## [1.10.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.9.1...v1.10.0) (2022-10-01)


### :sparkles: Features

* use pytest for tests ([78f0649](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/78f0649add1848dc1bf3495e0728e08b0e8dbc07)), closes [#1020992](https://gitlab.com/gitlabracadabra/gitlabracadabra/issues/1020992)


### :bug: Fixes

* **warnings:** fix fixture class name ([1564f3c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1564f3ca4d526eb8e7b345ff7dcc9a225f549424))
* **warnings:** fix fixture class name ([d79bb6d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d79bb6dbb7f906d8825223acf98c67eba7556638))
* **warnings:** fix server response differs from the user-provided base URL ([0f1afdb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0f1afdb620c4283644ef43d2f8a532a94041455f))
* **warnings:** replace assertRegexpMatches by assertRegex ([d410744](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d410744d548545671b090dfff881c32b8c7fd96f))


### :repeat: Chore

* **ci:** run commitlint job sooner ([e3b919f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e3b919fa8aa50b6293cc4e951b5eeb0e0f16fc51))
* **deps:** update dependency flake8-bugbear to v22.9.23 ([969f689](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/969f6897d96584094a628a9d78e97097ef8ba954))
* **deps:** update dependency flake8-bugbear to v22.9.23 ([62f59f9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/62f59f9d98f4700add691603ae3bf177184a8b36))
* **deps:** update dependency types-html5lib to v1.1.11 ([d3e220c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d3e220cdf2155ad05c7b3ab2d1d7661ae42bc0d4))
* **deps:** update dependency types-html5lib to v1.1.11 ([bfcb786](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bfcb786f505cc360f0bbf558c6218b2442220bac))
* **deps:** update node.js to v18.9.1 ([64450ae](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/64450ae1f90c23637f33af6ccec05c3886fa3581))
* **deps:** update node.js to v18.9.1 ([8d9fc8c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8d9fc8c1f6e4a114ef6a96c986aad5dd367f9103))


### :memo: Documentation

* update release docs ([dc08c5a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dc08c5a32220a823ce0344bf1cfeab51b8b359bb))

## [1.9.1](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.9.0...v1.9.1) (2022-09-20)


### :repeat: Chore

* **ci:** expire artifacts in 8 weeks ([f17b1fe](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f17b1fea928992dec47c40b1f8d8d4601af3af85))
* **deps:** update dependency flake8-bugbear to v22.9.11 ([9f7a880](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9f7a8804ed8d8e8bacea389e1737dd76bdc0a90b))
* **deps:** update dependency flake8-bugbear to v22.9.11 ([96beccd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/96beccdee17b69639d3da7d6c635c1397d6c6834))
* **deps:** update dependency jsonschema to v4.15.0 ([55b234a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/55b234afdd2b714f4422321ab5e9eb2ecf0cced3))
* **deps:** update dependency jsonschema to v4.15.0 ([89596b5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/89596b578ad9e5183f7dc8260ffd0c9ba81057fb))
* **deps:** update dependency jsonschema to v4.16.0 ([b24407b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b24407b44e158a90b31ed4566307b53ccbe59baa))
* **deps:** update dependency jsonschema to v4.16.0 ([1f0047d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1f0047d45530c03c858077a5a6407f772438107f))
* **deps:** update dependency pylint to v2.15.2 ([618873d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/618873d89bc15cd886da7859a2525c651a1f4c6a))
* **deps:** update dependency pylint to v2.15.2 ([0b46ac7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0b46ac7edb53b5d0b0acc2973dd4817b3e4ccca4))
* **deps:** update dependency pylint to v2.15.3 ([6cbda9b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6cbda9be4a4eb1087224bdb77b764beab04fa2fb))
* **deps:** update dependency pylint to v2.15.3 ([853115d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/853115da5b05e9e94428018e56096bf087a13e0c))
* **deps:** update dependency types-requests to v2.28.10 ([ee387c5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ee387c56c64341fdfad1444c720600d0782e39db))
* **deps:** update dependency types-requests to v2.28.10 ([04dba69](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/04dba69c6203d56398ce02b196e526b5f433cba5))
* **deps:** update dependency vcrpy to v4.2.1 ([5462f41](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5462f41a37e4668602301460896187636b3fdc82))
* **deps:** update dependency vcrpy to v4.2.1 ([d278065](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d278065a7a1f54b47a738613a86d4a9907062b97))
* **deps:** update node.js to v18.9.0 ([639044f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/639044feeaade7ed7de88ec9683abc54f77ec83f))
* **deps:** update node.js to v18.9.0 ([323f2b7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/323f2b7037475a5e47519a85cbb892c340c3e72a))


### :bug: Fixes

* **tests:** skip application settings tests with python-gitlab < 3.7 ([d0a0c15](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d0a0c15b5b0b248edd8dce30e9507f5e252c083f))
* **tests:** skip application settings tests with python-gitlab < 3.7 ([0b6a0e8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0b6a0e8d2dcc0c4bcb5ce70859ddf24ae95c4844)), closes [#1020126](https://gitlab.com/gitlabracadabra/gitlabracadabra/issues/1020126)

## [1.9.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.8.1...v1.9.0) (2022-08-31)


### :sparkles: Features

* support all possible state transistions ([8897fdc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8897fdc813c716925629921165ead30782106801))
* support all possible state transistions ([bd91f70](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bd91f7078a6334a8d8682cfd65b0c3d611240f04))


### :bug: Fixes

* **helm:** issue a warning instead of an info when package is not found ([708fb4a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/708fb4aab32bd29ebd1ccacd9b79ecfebd2599fa))
* **helm:** issue a warning instead of an info when package is not found ([0593ca2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0593ca21adfbf1e9364e086a84c6b76fee625dd9))
* **image_mirrors:** set user-agent ([678216e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/678216e2290fab1c37bc8e9f5338869d00d32deb))
* **image_mirrors:** set user-agent ([6486cdc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6486cdcc4fdea8c06d1615fed55c41d2bcbed83a))


### :repeat: Chore

* **deps:** remove explicit dependency on pep8-naming ([6c7ec1d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6c7ec1d559fac9e8a2f9300a6f8add575f0ba726))
* **deps:** update dependency coverage to v6.4.2 ([a67166a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a67166ac025bbe9b094bd78efc05f8ca317a79e9))
* **deps:** update dependency coverage to v6.4.2 ([fd04df9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fd04df98e055179c7aabfa94c61af3b644e78156))
* **deps:** update dependency coverage to v6.4.3 ([c1b1e6a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c1b1e6ad140b76410687a816cbc291c397dc33b0))
* **deps:** update dependency coverage to v6.4.3 ([59728b6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/59728b6f1c00213690c2f7b4111c970b92cff554))
* **deps:** update dependency coverage to v6.4.4 ([cbe7b81](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cbe7b8158dbfbbeb928704ea705b16a5d717e240))
* **deps:** update dependency coverage to v6.4.4 ([2749ac6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2749ac69775b770ae8692c45c1af7d29470d791f))
* **deps:** update dependency flake8-bugbear to v22.7.1 ([dc811db](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/dc811dbbfa10af639bd7fe2f43527bd497702b28))
* **deps:** update dependency flake8-bugbear to v22.7.1 ([61304a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/61304a32b0a9f8ce8c2cbe0ca618b410dad5292b))
* **deps:** update dependency flake8-bugbear to v22.8.22 ([38956fc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/38956fce165ad1c65c5d0465a9edaa6acc8d1450))
* **deps:** update dependency flake8-bugbear to v22.8.22 ([92005ec](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/92005ec9043e475bcbf6e828ca5720f237084138))
* **deps:** update dependency flake8-bugbear to v22.8.23 ([6206d55](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6206d559d49da96cb2fae78cd64e6e320e87d2c7))
* **deps:** update dependency flake8-bugbear to v22.8.23 ([3e50261](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3e5026166b262adc0fd61c135bdaf9f76acc556c))
* **deps:** update dependency flake8-logging-format to v0.7.4 ([c58c205](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c58c205447ea5bf94cfc38f06b6a9064055b3241))
* **deps:** update dependency flake8-logging-format to v0.7.4 ([fcc9f7b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fcc9f7bc170def86176fc40558c608f40c20d129))
* **deps:** update dependency flake8-logging-format to v0.7.5 ([4cbba72](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4cbba7293347ebdcc6614b28f311089e072690fb))
* **deps:** update dependency flake8-logging-format to v0.7.5 ([8989949](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/89899497311c47b30b07f63cfa6db8a99c35af84))
* **deps:** update dependency flake8-rst-docstrings to v0.2.7 ([838416b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/838416bf394eb09b27244225042bac03c21fcbc2))
* **deps:** update dependency flake8-rst-docstrings to v0.2.7 ([9713083](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9713083c593f03d63425db79bb8294d540c165e6))
* **deps:** update dependency jsonschema to v4.14.0 ([69fcc8d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/69fcc8dbe6a12825d4d3a2262a9b2d25a80aba8c))
* **deps:** update dependency jsonschema to v4.14.0 ([7a1b811](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7a1b8119431dd8625a7872af32ad93841028f84a))
* **deps:** update dependency jsonschema to v4.6.2 ([d76c2de](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d76c2dea4d6f65cfb1df8ee14828b696e1852d4a))
* **deps:** update dependency jsonschema to v4.6.2 ([d64eb4c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d64eb4c1c6ea7ce972e6ad260427a0d4738fd257))
* **deps:** update dependency jsonschema to v4.7.2 ([9804e2e](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9804e2e1247cee92dcf52cb5b32cb072bea3ec98))
* **deps:** update dependency jsonschema to v4.7.2 ([338e85f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/338e85f447f62519509fac48bc679a4b0f64187b))
* **deps:** update dependency jsonschema to v4.9.0 ([98e6456](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/98e6456fcf597f22c16c57a489259a0fbe4e1d81))
* **deps:** update dependency jsonschema to v4.9.0 ([9a0d698](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9a0d698ee86633fe1b9e9869ce177c627c560216))
* **deps:** update dependency jsonschema to v4.9.1 ([18fb8a3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/18fb8a32e91746e68f832bd9fb5a4518b6f501c7))
* **deps:** update dependency jsonschema to v4.9.1 ([09adad9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/09adad9a2a290ca99a296fa77e3bdad5f80f7e03))
* **deps:** update dependency mypy to v0.971 ([2338efa](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2338efa4273c7bc6a54e26622e70490d21faab3f))
* **deps:** update dependency mypy to v0.971 ([49e69dd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/49e69dd0b83a9a679ea7d43e1ec7164f564a985a))
* **deps:** update dependency pep8-naming to v0.13.0 ([e53c0c0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e53c0c047d6aa6d01a3db27c46c9ea634745d2d4))
* **deps:** update dependency pep8-naming to v0.13.0 ([c068754](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c0687545d2c461cca9574bb5387b9ee52df81e32))
* **deps:** update dependency pylint to v2.14.4 ([3c97aa8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3c97aa89689d4a279e129e5a2d507bf385b2984e))
* **deps:** update dependency pylint to v2.14.4 ([d6b1b7c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d6b1b7c3a48e5ee784b2a23cc7fb8431fe498af9))
* **deps:** update dependency pylint to v2.14.5 ([eef1c54](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eef1c5419dc9f05227a82a6d46c36cc68071aa0d))
* **deps:** update dependency pylint to v2.14.5 ([224cbc5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/224cbc502cfcb0231cda93713ddd371a71f805b5))
* **deps:** update dependency pylint to v2.15.0 ([a39d325](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a39d325c2faf80f589c96754282ae259e1b89af8))
* **deps:** update dependency pylint to v2.15.0 ([2dc1f11](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2dc1f111e7e3da69550c4bd9ef6e58b886ce5e02))
* **deps:** update dependency python-gitlab to v3.9.0 ([fdeffdb](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fdeffdbe8471e204f9117db6145e825c8dee41b3))
* **deps:** update dependency python-gitlab to v3.9.0 ([38fc6a4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/38fc6a45ab9e5bd85f20d211b8f2b062000e1a8c))
* **deps:** update dependency types-html5lib to v1.1.10 ([39f0523](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/39f05236a7c2c5c5e95cec7f6ba70ec3635eddb2))
* **deps:** update dependency types-html5lib to v1.1.10 ([a391431](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a3914311e6294319cd0baac563192dd21337d1fe))
* **deps:** update dependency types-html5lib to v1.1.8 ([a7003e9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a7003e9a1b4133c3fde1c1689e10cc0ed5810bcd))
* **deps:** update dependency types-html5lib to v1.1.8 ([471c50a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/471c50a1908c9d22f1f4703d37588ba6292e2139))
* **deps:** update dependency types-html5lib to v1.1.9 ([9f3e94a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9f3e94a65f4f26d72ccae3dd347dbe8e7c8b794c))
* **deps:** update dependency types-html5lib to v1.1.9 ([4df7d27](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4df7d27348b28c745856abec6ccfbcbb9bbe48cc))
* **deps:** update dependency types-pyyaml to v6.0.10 ([83f8ba1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/83f8ba17d9904dae99c251e73729d6692948d7cb))
* **deps:** update dependency types-pyyaml to v6.0.10 ([6204c9a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6204c9a5903b2a4e405a042480e6c26de2e33d89))
* **deps:** update dependency types-pyyaml to v6.0.11 ([f6604f5](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f6604f5d43807d0286c48d4e78d6b6277d32cd31))
* **deps:** update dependency types-pyyaml to v6.0.11 ([48d0159](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/48d01597fa68048d9e1e4b4dcc141daedbc142dd))
* **deps:** update dependency types-requests to v2.28.2 ([d95e8f9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d95e8f92813c3d7ed0b2d6afc7904a139bd3d035))
* **deps:** update dependency types-requests to v2.28.2 ([c693e6d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c693e6d40d4fb48c4e225f7047ebed9eefcbfb9e))
* **deps:** update dependency types-requests to v2.28.3 ([c5d2185](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c5d21857d69c04e18ba70b9b4b5368292c81e5ab))
* **deps:** update dependency types-requests to v2.28.3 ([f288bc3](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f288bc3d230ab83e1d05637a06ef231b9846770c))
* **deps:** update dependency types-requests to v2.28.7 ([9a8cddd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9a8cddd9bfd1ecea26b47c202b9562497e6bc3c7))
* **deps:** update dependency types-requests to v2.28.7 ([2765582](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2765582042dbdf6911f83542bc0b75b72d998ab5))
* **deps:** update dependency types-requests to v2.28.8 ([0c376dd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0c376dd0b02769f6235b07d938674f0f73d99a89))
* **deps:** update dependency types-requests to v2.28.8 ([b5cead2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b5cead28d5568e4a69058ea5436e663be1ccc45f))
* **deps:** update dependency types-requests to v2.28.9 ([309e660](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/309e6607a53b75df646a34d280696468cb76fb76))
* **deps:** update dependency types-requests to v2.28.9 ([0ede4e0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0ede4e025d49ef3b21e2305829e587a2c65da091))
* **deps:** update dependency vcrpy to v4.2.0 ([f284e04](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f284e040501dfd6e73331615c8af025b078cad4d))
* **deps:** update dependency vcrpy to v4.2.0 ([d63bb81](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/d63bb8162db61f55f67cd81f47efe1b86d24c34f))
* **deps:** update node.js to v18.6.0 ([c681fd6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c681fd6eead254a3c08261f71b4ee008a1df3dfb))
* **deps:** update node.js to v18.6.0 ([617b78a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/617b78a858db151ce4df25fef058a939f8db21a6))
* **deps:** update node.js to v18.7.0 ([3e11ee8](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3e11ee84b5fc72fd641f257c9365c33b7a33519e))
* **deps:** update node.js to v18.7.0 ([9c0347f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9c0347f1b3b2b1a4512ad6253e1435933319fcca))
* **deps:** update node.js to v18.8.0 ([6ad1340](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6ad134017e7ac95e0a39a1be2097ffc0cddb7a47))
* **deps:** update node.js to v18.8.0 ([8136197](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/81361973f1019c3fcbfe505b87a9e8cb38eda861))
* **deps:** update tests for python-gitlab >= 3.7 ([81449a1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/81449a187aa6effa8f98d8b32b6a5821626b737f))

## [1.8.1](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.8.0...v1.8.1) (2022-06-29)


### :memo: Documentation

* update release instructions ([eadf5aa](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eadf5aaa9fecfe3659385bb803ea0beff3f5a48f))


### :repeat: Chore

* **debian:** add gbp.conf for default branch ([07cb354](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/07cb354c21122c459a9c770f228f2fdf92e1a6ca))
* **debian:** update tar-ignore list ([fda96c6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fda96c6d82cbdc9e316d28c889669807d738878d))
* **release:** add changelog title ([195aa95](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/195aa9515ad58e2da33e4718dc109d04be2ef941))


### :question: Unclassified

* Release debian ([6fd1749](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6fd1749fca738d7fc66793e6c59d8452d3b88915))

## [1.8.0](https://gitlab.com/gitlabracadabra/gitlabracadabra/compare/v1.7.0...v1.8.0) (2022-06-29)


### :question: Unclassified

* Development mode for 1.8.0 ([0e48396](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0e48396c3a38f21d16e55445bc5a6919db4ce74d))
* Development mode for 1.8.0 ([baa3408](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/baa34087bbe25367462fc9a4a3b408e796f3cfe9))


### :memo: Documentation

* development environment ([985d086](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/985d08689dff3d7f3d633e61c7614edc3beb8bf6))
* development environment ([2cf3fe0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2cf3fe0729fff77ea845d47da8be01c3f2e8dc00))
* improve contribution docs and add commitlint ([c8c53a6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c8c53a669ea9abfe4e9a303f96bd23ad97474262))
* improve contribution docs and add commitlint ([fb7ec97](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/fb7ec97dea4582ea5840b5c0417786ea45c00c75))


### :sparkles: Features

* **registry:** fix auth with redirect ([b1cd348](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b1cd3482bf9583c5db863c359e12cafcdb7119bf))
* **registry:** fix auth with redirect ([47a87f9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/47a87f9ffdf794da75bd50c49cc6f95dff240868))
* rename default branch from master to main ([70941c2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/70941c2232f1278ade1e97ff0b0a37fae633f69e))


### :bug: Fixes

* update gitlabracadabra.yml to current state ([e2e128d](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e2e128d28751d11df190ac9e80c0d103faf4e619))


### :repeat: Chore

* **deps:** update dependency coverage to v6.3.3 ([da8e632](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/da8e63259d3c152a67d4823e07a815f24f246d2f))
* **deps:** update dependency coverage to v6.3.3 ([72eb633](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/72eb633fd6b425ecc06e271d16b3eec6a2707054))
* **deps:** update dependency coverage to v6.4 ([a6bf80a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a6bf80a39f1228c7cc46dbda78419f24fdea0ec2))
* **deps:** update dependency coverage to v6.4 ([3f98ae0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3f98ae0d15d021a1418283a335c2cdf5a7216cae))
* **deps:** update dependency coverage to v6.4.1 ([3b38e94](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3b38e947b2c3d2ee71563a01b6843e378a86db6f))
* **deps:** update dependency coverage to v6.4.1 ([12f64fc](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/12f64fc5e113a07326f564777491ebe92a9038c6))
* **deps:** update dependency flake8-bugbear to v22.6.22 ([351f068](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/351f0688b72e60b68bb5f17eec3b70b9e700c933))
* **deps:** update dependency flake8-bugbear to v22.6.22 ([ac9fd91](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ac9fd9166877b92a5839d06484ac8deb222d4a67))
* **deps:** update dependency flake8-comprehensions to v3.10.0 ([73789ca](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/73789cafeb185c86dc4bd7958acc2fe09d11f4a3))
* **deps:** update dependency flake8-comprehensions to v3.10.0 ([4bc5d6c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4bc5d6ce5d0756b21f17cd778f0e7d6537d8be31))
* **deps:** update dependency flake8-comprehensions to v3.9.0 ([eadbb41](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eadbb41adcb52feabb199821f82ad22b1726756b))
* **deps:** update dependency flake8-comprehensions to v3.9.0 ([641e012](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/641e0127390d413d5ac9dc9d0955b63fb4839201))
* **deps:** update dependency flake8-rst-docstrings to v0.2.6 ([4d97c51](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/4d97c5121aee1a8916b63721821346c54b278f94))
* **deps:** update dependency flake8-rst-docstrings to v0.2.6 ([07775af](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/07775af01a38a6e29dc15f58f2d6efbe2afd8c03))
* **deps:** update dependency flake8-tidy-imports to v4.7.0 ([8c7ef60](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/8c7ef6084c9a6a6c3dc82c4138d1fa0f3ea17bef))
* **deps:** update dependency flake8-tidy-imports to v4.7.0 ([a770e0c](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a770e0c15d9aa8dd0bdc41784182a0f30053e06d))
* **deps:** update dependency flake8-tidy-imports to v4.8.0 ([0de2c83](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0de2c833cd498bbec8dbaf1f2fb98d8d04d1dfb8))
* **deps:** update dependency flake8-tidy-imports to v4.8.0 ([e2ebf16](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e2ebf1613f6dd0d163187d8577041f5116207a8c))
* **deps:** update dependency jsonschema to v4.6.0 ([a01b161](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a01b16135585a843b2d3ff3ae88e8e479c4fe255))
* **deps:** update dependency jsonschema to v4.6.0 ([cfec8c6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/cfec8c663099aedb0d86ef38cfdcffc60e7d40b1))
* **deps:** update dependency jsonschema to v4.6.1 ([a554e98](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a554e98c4d503df1e42b2e45fdeaa6bbfe6d0224))
* **deps:** update dependency jsonschema to v4.6.1 ([b4efe38](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b4efe38cbb5bd24c1ec67727c22dff94b08dc19b))
* **deps:** update dependency mypy to v0.960 ([f605cad](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f605cadcce6313354096d7a321f59126f2e032d8))
* **deps:** update dependency mypy to v0.960 ([59e8601](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/59e86015f4fbfc0332251af99fd66497612525e0))
* **deps:** update dependency mypy to v0.961 ([0afa959](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0afa9594d62211c34883c0e4a04d101460dbf8ea))
* **deps:** update dependency mypy to v0.961 ([c519925](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c5199250ed55092446cfef6538f6e40f2b22326e))
* **deps:** update dependency pylint to v2.13.9 ([72b40ea](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/72b40ea7223d92ba13cc59b823957a932c0dd120))
* **deps:** update dependency pylint to v2.13.9 ([b0fdc04](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b0fdc041e79b0d7167a911262e91c60fca13872b))
* **deps:** update dependency pylint to v2.14.0 ([3232235](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3232235ab15f48268c57169567d07b4e172e7e97))
* **deps:** update dependency pylint to v2.14.0 ([e18cd1b](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e18cd1bb7326c712e7502a6aea182a64faac24f7))
* **deps:** update dependency pylint to v2.14.1 ([0e345a7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/0e345a7e05b3e9457b40c6ea232aa98ab967b43d))
* **deps:** update dependency pylint to v2.14.1 ([38b0b92](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/38b0b9226e611fc691091b3e268da0b1ce1a7556))
* **deps:** update dependency pylint to v2.14.2 ([3f815f0](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/3f815f05744702c58273c59b3badcf36ced50228))
* **deps:** update dependency pylint to v2.14.2 ([ff03c77](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ff03c77fd819e77cb60cd102027c43c2fa9851c2))
* **deps:** update dependency pylint to v2.14.3 ([1862cf6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/1862cf6f499fadf64646272a44ca17853f5ee4d0))
* **deps:** update dependency pylint to v2.14.3 ([5901c41](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/5901c41a8acebef123f9594df8ee8fa3a312921b))
* **deps:** update dependency python-gitlab to v3.5.0 ([542dc4a](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/542dc4aadb8855ce53f39f8845f8cec0e4851b46))
* **deps:** update dependency python-gitlab to v3.5.0 ([6a81e69](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/6a81e69452d609a0fe96bba4ab0c6aecce704eff))
* **deps:** update dependency python-gitlab to v3.6.0 ([e8616bd](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e8616bd8cde5bf8d68ebbd14fce743550b8a5d5e))
* **deps:** update dependency python-gitlab to v3.6.0 ([7277087](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/7277087aca035323143dfb1d70dbde06fc0f4383))
* **deps:** update dependency semantic_version to v2.10.0 ([a881708](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/a88170885d93fdac7dbb794e5da159527bdf332b))
* **deps:** update dependency semantic_version to v2.10.0 ([f78d7e6](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/f78d7e69dcf7915dc8326c28bd904ea1fd975606))
* **deps:** update dependency types-pyyaml to v6.0.8 ([ed6553f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ed6553f46a42a342b6590f1a3e402f78a9e53913))
* **deps:** update dependency types-pyyaml to v6.0.8 ([9071af1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9071af138874b592201cd98debab91fff9a4a943))
* **deps:** update dependency types-pyyaml to v6.0.9 ([049a4da](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/049a4da8737013202f5896c5abfd6f859678db95))
* **deps:** update dependency types-pyyaml to v6.0.9 ([bf35536](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/bf35536a1245eeefb439ad1d9f65925897802525))
* **deps:** update dependency types-requests to v2.27.26 ([e224467](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e224467b2c21fd01f51dc1c37467cda11c7358c2))
* **deps:** update dependency types-requests to v2.27.26 ([eaad137](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/eaad137de33f457ad42e83dd4a7705ef6d9f789c))
* **deps:** update dependency types-requests to v2.27.27 ([2793647](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/27936475b5dd542287a5c2cbe20534732e1a34a1))
* **deps:** update dependency types-requests to v2.27.27 ([05820b4](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/05820b4e2cbeff886d11a1e82fe1690c9b035ef3))
* **deps:** update dependency types-requests to v2.27.28 ([da01e67](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/da01e671b06ba1587f476128a67c65a20929fa1f))
* **deps:** update dependency types-requests to v2.27.28 ([61bf5c1](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/61bf5c130b50ee808afaed41ad861c06b8d15459))
* **deps:** update dependency types-requests to v2.27.29 ([593ea63](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/593ea6374e4cd11a56dd36be85b33345fa4bd84e))
* **deps:** update dependency types-requests to v2.27.29 ([e003ace](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/e003acebbdad234bca3e6f2b476ebca3f6b850ee))
* **deps:** update dependency types-requests to v2.27.30 ([050a457](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/050a457e5d888a12e13e61456c3eb9cde5b67a05))
* **deps:** update dependency types-requests to v2.27.30 ([9703c32](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/9703c3258950bac6aea1417751ee4c0f8dd239a3))
* **deps:** update dependency types-requests to v2.27.31 ([62cd83f](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/62cd83fdf9b9804036e29415f1f2726c1d617d80))
* **deps:** update dependency types-requests to v2.27.31 ([2a80208](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/2a802088650d5ababf2a61d9619e733bbcd14abf))
* **deps:** update dependency types-requests to v2.28.0 ([b9b67a9](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/b9b67a99e3607e783a191ec7c5b2cb61e4700b51))
* **deps:** update dependency types-requests to v2.28.0 ([90e3731](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/90e373103b32f174ccbdff81f30980eb10e91c2b))
* future releases with semantic-release ([afd1bd7](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/afd1bd7d678e0168179ba377e82cea4a6bd70e74))
* future releases with semantic-release, and rename default branch from master to main ([c78dae2](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/c78dae2e61d679b404ef055b98072b4f71a3aaac))
* **parser:** fix C417  Unnecessary use of map ([ef10d19](https://gitlab.com/gitlabracadabra/gitlabracadabra/commit/ef10d190efdadd06848a8496d292ccac0559e3a7))

## 1.7.0 (2022-05-06)

**Enhancements:**

- Use Version.coerce instead of the strict version

**Tooling:**

- From `renovate`:
  - chore(deps): update dependency types-requests to v2.27.22
  - chore(deps): update dependency mypy to v0.950
  - chore(deps): update dependency python-gitlab to v3.4.0
  - chore(deps): update dependency types-requests to v2.27.24
  - chore(deps): update dependency types-requests to v2.27.25
  - chore(deps): update dependency flake8-debugger to v4.1.2
  - chore(deps): update dependency pylint to v2.13.8
  - chore(deps): update dependency jsonschema to v4.5.1## 1.6.0 (2022-04-26)

## 1.6.0 (2022-04-26)

**Enhancements:**

- urllib3: Lower retry message from WARNING to INFO (!295)

**Tooling:**

- tests: Use temp config instead of mocking (!303)
- From `renovate`:
  - chore(deps): update dependency types-requests to v2.27.19 (!298)
  - chore(deps): update dependency types-html5lib to v1.1.7 (!296)
  - chore(deps): update dependency types-pyyaml to v6.0.6 (!297)
  - chore(deps): update dependency pylint to v2.13.7 (!299)
  - chore(deps): update dependency types-pyyaml to v6.0.7 (!300)
  - chore(deps): update dependency types-requests to v2.27.20 (!301)
  - chore(deps): update dependency flake8-bugbear to v22.4.25 (!302)

## 1.5.0 (2022-04-13)

**Enhancements:**

- retry package connections 3 times (!265)
- Support regexp for Helm package name (!266)
- registry importer: Warn for all HTTP errors (#46, !267)
- python-gitlab v3 compatibility (!240, Debian's [#1009428](https://bugs.debian.org/1009428))

**Tooling:**

- Pin bandit to 1.7.2 (!268)
- Fix A504  prefer the 'msg=' kwarg for assertTrue() diagnostics (!276)
- python-gitlab v3 compatibility (!240)
  - Add TestCase.gitlab_version() to compare pygitlab version
  - python-gitlab v3: config handling has changed again
  - python-gitlab v3: Some urls changed
  - Update application settings schema
  - Always change values when processing a dependency
  - Update Application settings tests for pygitlab v3
- From `renovate`:
  - chore(deps): update dependency types-requests to v2.27.9 (!261)
  - chore(deps): update dependency types-requests to v2.27.10 (!262)
  - chore(deps): update dependency coverage to v6.3.2 (!263)
  - chore(deps): update dependency types-requests to v2.27.11 (!264)
  - Update dependency types-requests to v2.27.12 (!272)
  - Update dependency wemake-python-styleguide to v0.16.1 (!273)
  - Update dependency mypy to v0.940 (!274)
  - Update dependency mypy to v0.941 (!275)
  - Update dependency types-PyYAML to v6.0.5 (!277)
  - Update dependency flake8-bugbear to v22 (!247)
  - Update dependency types-requests to v2.27.13 (!278)
  - Update dependency flake8-assertive to v2.1.0 (!276)
  - Update dependency bandit to v1.7.4 (!279)
  - Update dependency flake8-bandit to v3 (!270, !279)
  - Update dependency flake8-blind-except to v0.2.1 (!280)
  - Update dependency types-requests to v2.27.14 (!281)
  - Update dependency flake8-bugbear to v22.3.20 (!282)
  - Update dependency flake8-bugbear to v22.3.23 (!283)
  - Update dependency mypy to v0.942 (!284)
  - Update dependency pylint to v2.13.0 (!285)
  - Update dependency pylint to v2.13.2 (!286)
  - Update dependency types-requests to v2.27.15 (!287)
  - Update dependency pylint to v2.13.3 (!288)
  - Update dependency pylint to v2.13.4 (!289)
  - Update dependency types-requests to v2.27.16 (!290)
  - Update dependency types-html5lib to v1.1.6 (!291)
  - Update dependency pylint to v2.13.5 (!292)
  - chore(deps): update dependency python-gitlab to v3 (!240)

## 1.4.0 (2022-02-08)

**Enhancements:**

- pypi: Always sync yanked packages when using equal specifier (!229)
- Fix Python GitLab link in doc (!215, #44)
- Update gitlabracadabra.yml with latest parameters (!222)
- retry registry connections 3 times (!257)

**Tooling:**

- Fix WPS324 Found inconsistent `return` statement (!233)
- Fix isort (!213)
- renovate: Remove hourly rate limit for PR creation (!234)
- From `lintian-brush`:
  - Bump debhelper from old 12 to 13 (!208)
  - debian/rules: Drop --fail-missing argument to dh_missing, which is now the default (!208)
  - Update standards version to 4.6.0, no changes needed (!208)
  - Remove constraints unnecessary since buster (!256)
- From `renovate`:
  - Update dependency coverage to v6.1.2 (!209)
  - Update dependency types-PyYAML to v6.0.1 (!210)
  - Update dependency PyYAML to v5.4.1 (!211)
  - Update dependency packaging to v21.3 (!212)
  - Update dependency wemake-python-styleguide to v0.15.3 (!213)
  - Update dependency PyYAML to v6 (!214)
  - Pin flake8-broken-line==0.3.0 (!213)
  - Pin flake8 (!213)
  - Pin pycodestyle (!213)
  - Pin pep8-naming (!213)
  - Update dependency types-requests to v2.26.1 (!216)
  - Update dependency coverage to v6.2 (!217)
  - Update dependency flake8-bugbear to v21.11.29 (!219)
  - Update dependency flake8-rst-docstrings to v0.2.5 (!220)
  - Update dependency types-html5lib to v1.1.3 (!223)
  - Update dependency types-requests to v2.26.2 (!224)
  - Update dependency jsonschema to v4.3.2 (!225)
  - Update dependency mypy to v0.930 (!226)
  - Update dependency jsonschema to v4.3.3 (!227)
  - Update dependency types-html5lib to v1.1.4 (!228)
  - chore(deps): update dependency pylint to v2.12.2 (!231)
  - chore(deps): update dependency types-requests to v2.27.0 (!232)
  - chore(deps): update dependency wemake-python-styleguide to v0.16.0 (!233)
  - chore(deps): update dependency flake8-broken-line to v0.4.0 (!218)
  - chore(deps): update dependency pep8-naming to v0.12.1 (!221)
  - chore(deps): update dependency types-html5lib to v1.1.5 (!235)
  - chore(deps): update dependency types-requests to v2.27.1 (!237)
  - chore(deps): update dependency flake8-assertive to v2 (!239)
  - chore(deps): update dependency types-pyyaml to v6.0.2 (!236)
  - chore(deps): update dependency flake8 to v4 (!238)
  - chore(deps): update dependency pycodestyle to v2.8.0 (!238 superseding !230)
  - chore(deps): update dependency types-requests to v2.27.5 (!243)
  - chore(deps): update dependency mypy to v0.931 (!244)
  - chore(deps): update dependency types-pyyaml to v6.0.3 (!242)
  - chore(deps): update dependency flake8-comprehensions to v3.8.0 (!245)
  - chore(deps): update dependency flake8-tidy-imports to v4.6.0 (!246)
  - chore(deps): update dependency types-requests to v2.27.6 (!248)
  - chore(deps): update dependency jsonschema to v4.4.0 (!249)
  - chore(deps): update dependency types-requests to v2.27.7 (!250)
  - chore(deps): update dependency coverage to v6.3 (!251)
  - chore(deps): update dependency types-pyyaml to v6.0.4 (!253)
  - chore(deps): update dependency types-requests to v2.27.8 (!254)
  - chore(deps): update dependency coverage to v6.3.1 (!255)
  - chore(deps): update dependency semantic_version to v2.9.0 (!258)

## 1.3.0 (2021-11-10)

**New features:**

- Mirror PyPI packages (!183, !184, !185, !192, !193)
- Add squash_option support (!189, !198)
- Add user state support (!197, #41)

**Fixes:**

- package mirrors:
  - Keep original request URL, to avoid too long filename (!186)
  - Consider 202 Accepted as upload success (!190)
  - Use default Accept-Encoding (!196)
- image mirrors:
  - Force syncing all platforms when digest is set (!194)
  - Do not register blobs from manifest lists (!195)
  - Catch manifest import errors (!200, #42)

**Tooling:**

- Do not build Universal Wheel anymore (!182)
- Fix pep8 since wemake update (!199)
- Add renovate config (!201)
  - Pin dependencies (!202)
  - Update dependency isort to v5 (!203)
  - Pin all python packages (!204)
  - Pin mypy dependencies (!204)
  - Pin CI images (!205)
- Build deb on bullseye (!197)

## 1.2.0 (2021-06-20)

**New features:**

- Helm chart repository support (!169, !170, !171, #38, #39, !174, !179)
  - With repo_url, package_name, versions, semver, limit and channel parameters

**Enhancements:**

- warn instead of info for not found and other non-ok status (!172)
- CI:
  - Fix mock_calls on buster (!173)
  - Fix ProxyError message on buster (!173)
  - Fix buster linting issue (!179)
  - Update to latest mypy and types-* packages (!176)
  - Fix mypy errors (!177)
  - PyGitlab now converts arrays to comma-separated list as string (!177)
  - Reduce noqa usage (!178)

**Fixes:**

- Fix format order in package file upload logging (!172)
- Warn on HTTP errors instead of failing (#39, !172)
- Fix github assets upload (!175)
- Ensure parameter is mangled on change. Fixes: first_day_of_week in
  application settings (!177)

## 1.1.0 (2021-05-21)

**New features:**

- :rocket: Package mirroring (#31):
  - Raw URLs to generic packages (!156)
  - Github releases to generic packages
    (#34, #35, #37, !159, !160, !161, !162, !163, !164, !165)

**Enhancements:**

- Improve JSONschema errors
- Image mirrors features:
  - Store Blob sizes to avoid uneeded HEAD request when printing stats (!153)
  - Better manifest v1 handling: Avoid 404 errors when printing stat and with
    mounted blobs (#33, !153, !154, !155)
- Misc:
  - Fix hadolint: Multiple consecutive `RUN` instructions (!153)
  - Update requirements for libgit2 1.0 transition (!156)

## 1.0.0 (2021-04-17)

**Important fixes:**

- **Security**: Ensure TLS certificates is verified with container registries (!139)

**Enhancements:**

- Image mirrors features:
  - Implement SemVer (!148)
  - Support manifest v1 (#28, !143, !145)
  - Add OCI Image Media Types (!149)
  - Allow to synchronize image by digest (!138)
  - Handle registries without Docker-Content-Digest header (!141)
  - Avoid checking signed manifest digest (#29, !146)
  - Increase the chunk size to 50MB (!142)
  - Improve heuristics:
    - Always GET manifests as they are small (!146)
    - Register seen blobs from manifests (!146)
    - Check if blob exists before uploading (!146)
    - Avoid unneeded HTTP query just to get size (!146)
  - Allow tag regexp for string-form from too (!147)
- Lower NOT Deleting %s (not found) from INFO to DEBUG (!144)

**Fixes:**

- Image mirrors fixes:
  - Avoid infinite stack trace with cache_path (!140)
  - Fix blob mount (#30, !146)
  - Handle token expiration (!150)
- Use hadolint/hadolint:latest-alpine for hadolint job (!138)

## 0.9.1 (2021-03-20)

**Enhancements:**

- Sync debian/source/options tar-ignore with .gitignore (!134)
- Add dep5 tests (autopkgtest) (!135)

## 0.9.0 (2021-03-19)

**New features:**

- :rocket: Docker image mirroring (#13):
  - Basic mirroring (!114, !117, !118, !119 , !120, !123)
  - Support selecting images by base+repositories+tags (!128)
  - Tag matching with regexps (!129)
  - Bug fixes (#27, !127)
- Support for multiple GitLab connections, including `mirrors` (!130, !131)

**Enhancements:**

- Use shorter default logging format (!125)
- Improve error when `extends:` is not found (!101)
- mirrors: Enable automatic proxy detection when available (#25, !115, !125)
- User: Add skip_reconfirmation, note (!116)
- Docker image:
  - Use Debian package on Docker image (!102)
    - Removes pip usage
    - Ensure certificate usage is centralized (no `/usr/local/lib/python3.7/dist-packages/certifi/cacert.pem`)
    - Use `bullseye`, as `buster` packages are older
  - Test the Docker image (!103)
- Documentation:
  - Add the missing `v` prefix in Docker image tag (!99)
  - Fix docker command, and mention other images (!105)
  - Document ability to mirror Git repositories (!108)
- Code cleanup and developpement tooling:
  - Improve pep8 (!98, !109, !110), !111
  - Use "build" job from AutoDevOps (!104)
  - Refactor test utils (!106)
  - Drop python2 support (!110)
  - Initial mypy checks (!114)

**Fixes:**

- Always remove trailing slash, even when type is specified (#23, !100)
- Fix: is_admin -> admin param for creation or update (#26, !112, !122)
- Create cache dir (#19, !121, !124)

## 0.8.0 (2021-02-09)

Thanks to Sébastien Heurtematte who contributed two merge requests (!91, !93)!

**New features:**

- Mirror branch mapping (!83)
- Project and Group Boards (!85, !86)
- Add support for Docker retention policy (!84)
- Add rename_branches parameter (!89)

**Enhancements:**

- Various pep8 improvements and added flake8 plugins (!87, !88)
- Enhance docker build and debug (!93)
- Improve CI (!95)
- Documentation:
  - Document "Using gitlabracadabra in GitLab CI" (!89)
  - doc: Add missing usage example in "Using packages" (!89)

**Fixes:**

- Use api param namespace_id to parent_id for subgroup creation (!91)

## 0.7.0 (2020-10-29)

**New features:**

- Implement group of groups (!74)
- Implement pipeline schedules (including variables) (!75)
- Add aggregate merge strategy (!77)

**Enhancements:**

- Document required release dependencies (!72)
- Update copyright to 2020 (!73)
- Fix Unknown merge strategy error message (!76)
- Consider null label description as same as empty (!78)
- Ship all documentation in deb package (!79)

## 0.6.0 (2020-06-18)

**Important fixes:**

- **Security**: Mirroring: Only send Private-Token on gitlab remote (!63)

**New features:**

- Implement project's webhooks (!67)

**Enhancements:**

- Add forking\_access\_level, pages\_access\_level, emails\_disabled and suggestion\_commit\_message
  to projects (!66, !69)
- Add support for project's allow\_merge\_on\_skipped\_pipeline and autoclose\_referenced\_issues (!70)
- tests: Ignore host and port on match (!67)
- Documentation:
  - Reorder project and group parameters as in WebUI (!65)
  - Update release doc (!62)
  - Sync README with requirements.txt (pygit2 is now needed) (!69)

**Fixes:**

- Pin pygit2 to 1.0.3 to fix FTBFS (!64)
- CI and tests fixes (!68):
  - Fix "jobs:dast config key may not be used with `rules`: except"
  - Fix "E712 comparison to False should be 'if cond is False:' or 'if not cond:'"
  - Fix "E302 expected 2 blank lines, found 1"

## 0.5.0 (2020-01-23)

**Backwards incompatible changes:**

- `extends` now use `deep` merging by default, to restore the previous behavior
  use `replace` merge strategy

**New features:**

- `extends` now supports two merge strategies: `deep` (the default) and `replace` (!54 and !56)
- `extends` now supports multiple parents (!56)
- Repository mirroring (!52, !60)
- Add support for project and group milestones (!58)

**Enhancements:**

- Add remove\_source\_branch\_after\_merge parameter to projects (!46)
- Add default\_ci\_config\_path parameter to application settings (!47)
- Add emails\_disabled, project\_creation\_level, subgroup\_creation\_level, require\_two\_factor\_authentication,
  two\_factor\_grace\_period, auto\_devops\_enabled to groups (!59)
- Sync with current project and group parameters (!59)
- Documentation:
  - Add Table of Contents (!48)
  - Generate markdown doc from source (!48)
  - Document releasing a new version (!49)
  - Various documentation enhancements (!51)
  - Add general action file documentation (!53)
- Tests:
  - Only install packages for the gitlabracadabra.tests.unit package (!55)

**Fixes:**

- User:
  - Fix typo of user param: `s/admin/is_admin/` (!57)
  - Remove username as param, as this is the id (!57)
  - Fix `CREATE_KEY` handling (!57)
  - user's skip_confirmation is a create param (!57)
  - Do not try to change create-only params for users (!57)
- Tests:
  - Clean up before testing group member not found (!50)

## 0.4.0 (2019-11-14)

**New features:**

- Add delete\_object param (!43)
- Add support for include (#7, !42)
- Add project and group labels support (#2, !36, !39, !40)
- Add support for application settings (#4, !34)
- Package uploaded to Debian (!45)

**Enhancements:**

- Add support for latest project params (!33)
- Test YAML file loading (!42)
- Ensure that content is not changed during processing (!41)
- User password is only set on user creation (!41)
- Assert all cassettes are "all played" (!37, !38)

**Fixes:**

- Don't fail when some variables params are not available (!44)
- Python-gitlab 1.13 still has incorrect labels support (!45)

## 0.2.1 (2019-06-20)

**New features:**

- Debian packages (!24, !25)
- Project and group variables (!29)
- Project archived parameter (!27)

**Enhancements:**

- Catch members deletion errors (!30)
- Skip changing parameter only when not available (i.e. handle null as current value) (!28)
- Catch REST errors of \_process\_ methods (!26)
- Enforce draft 4 of jsonschema (!25)
- Skip protected tags tests on python-gitlab < 1.7.0 (!24)
- Workaround "all=True" in API calls with python-gitlab < 1.8.0 (!24)
- Move VCR fixtures closer to the tests (!23)
- Centralize VCR configuration (!23)

## 0.1.0 (2019-05-25)

**New features:**

- Projects
  - Basic parameters
  - Project members
  - Add protected branches support
  - Protected tags
  - Add latest project parameters
    (mirror\_user\_id, only\_mirror\_protected\_branches, mirror\_overwrites\_diverged\_branches, packages\_enabled)
  - Add initialize\_with\_readme support to projects
- Groups:
  - Basic parameters
  - Group members
  - Add latest group parameters
    (membership\_lock, share\_with\_group\_lock, file\_template\_project\_id, extra\_shared\_runners\_minutes\_limit)
- Users:
  - Basic parameters
